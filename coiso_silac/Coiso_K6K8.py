#!/usr/bin/env python
# coding: utf-8

# In[1]: import the relevant packages for the script.

## run_calcisotopes is a subprocess to run a C++ executable. From coiso silac method we output the fragment/peptide chemical composition to stdin for the exe and output a list of the theoretical distribution (std out) across the monoisotopic and the 1st 4 isotope errors
### composition input into exe is in the format [Carbon, Hydrogen, Nitrogen, Oxygen, Sulfur]

import sys
from pyteomics import mzml   
import pandas as pd
import re
import numpy as np
import matplotlib.pyplot as plt
from pyteomics import mgf
import mokapot
from re import search
from numpy import median
from datetime import datetime
from sklearn.linear_model import LinearRegression
from itertools import repeat
import json
import subprocess
import statistics
import functools
from pkg_resources import resource_filename

calcisotopes_path = resource_filename(__name__, 'calcisotopes/calcisotopes.exe')
@functools.lru_cache(maxsize=None) 
def run_calcisotopes(comp): 
    theo_dist=subprocess.run([calcisotopes_path, comp], capture_output=True) 
    return theo_dist.stdout.decode("utf-8")

# In[2]:

## aa_masses_dict is a dictionary for the monoisotopic masses when added to peptide bond. Carbidomethyl mass addition is added to cysteine's monoisotopic. The lysine 6 is currently the light version is hard coded with lysine monoisotopic + 6Da

aa_masses_dict = {'G': 57.021463735,
'A': 71.037113805,
'S': 87.032028435,
'P': 97.052763875,
'V': 99.068413945,
'T': 101.047678505,
'C': 160.030648505,## with c+57  103.009184505 + 57.021464  160.030648505
'L': 113.084064015,
'I': 113.084064015,
'N': 114.04292747,
'D': 115.026943065,
'Q': 128.05857754,
'K': 134.11509206, ### K6
'E': 129.042593135,
'M': 131.040484645,
'H': 137.058911875,
'F': 147.068413945,
'R': 156.10111105,
'Y': 163.063328575,
'W': 186.07931298}

## Comp_masses_dict is a dictionary of all the amino acid's atom composition in the following format [Carbon, Hydrogen, Nitrogen, Oxygen, Sulfur]. This includes carbidomethyl's atom contributions to cysteine. K6 lysine is the same as K0 for composition here.

Comp_masses_dict = {'G' : [2 , 3  , 1  , 1 ,0],
'A' : [3 , 5  , 1  , 1 ,0],
'S' : [3 , 5  , 1 , 2 ,0],
'P' : [5 , 7  , 1  , 1 ,0],
'V' : [5 , 9  , 1  , 1 ,0],
'T' : [4 , 7  , 1  , 2 ,0],
'C' : [5, 8,2,2,1], # carb +57    'C' : [3+2 5+3, 1+1, 1+1,1]
'L' : [6 , 11 , 1 ,1 ,0],
'I' : [6 , 11 , 1  , 1 ,0],
'N' : [4 , 6  , 2 ,2 ,0],
'D' : [4 , 5  , 1  , 3 ,0],
'Q' : [5 , 8  , 2 , 2 ,0],
'K' : [6 ,12 , 2 , 1 ,0], #K6  : [6 , 12 , 2 , 1 ,0],
'E' : [5 , 7  , 1  , 3 ,0],
'M' : [5 , 9  , 1  , 1 ,1],
'H' : [6 , 7  , 3 , 1,0],
'F' : [9 , 9  , 1  , 1 ,0],
'R' : [6 , 12 , 4 , 1 ,0],
'Y' : [9 , 9  , 1  , 2 ,0],
'W' : [11, 10 , 2 , 1 ,0]}

### Parse MS/MS data from mzML for relevant MS/MS-based annotation of paired y-ion features and b-ion features and quantification of convoluted paired y-ion isotopic profiles

def parse_scan(s):
    scan_dict={}
    if s["ms level"] == 1:
        return None
    #(Scan Number)
    scan_dict["ScanNum"]=s["index"]+1 
    #MS level
    scan_dict["MSlevel"]=s["ms level"]
    #(type of scan, selected ion location (not original selected precursor), CE, and window size) 
    scan_dict["ScanInfo"]=s["scanList"]["scan"][0]["filter string"]  
    ##retention time
    scan_dict["RetentionTime"]=s["scanList"]["scan"][0]["scan start time"]
    #Precursor detected that is triggering the offset from the MS1
    scan_dict["PrecursorDetected"]=s["precursorList"]["precursor"][0]["selectedIonList"]["selectedIon"][0]["selected ion m/z"]
    #Center of isolation window target
    scan_dict["MS2TargetMass"]=s["precursorList"]["precursor"][0]["isolationWindow"]["isolation window target m/z"]
    #Isolation window size
    scan_dict["IsolationSize"]=s["precursorList"]["precursor"][0]["isolationWindow"]["isolation window lower offset"]*2
    #charge state
    scan_dict["Charge"]=s["precursorList"]["precursor"][0]["selectedIonList"]["selectedIon"][0]["charge state"] 
    #Array of m/z's
    scan_dict["m/zArray"]=s["m/z array"]
    #Array of respect m/z intensities
    scan_dict["IntensityArray"]=s["intensity array"]
    #MS2 base peak
    scan_dict["MS2BasePeak"]=s["base peak m/z"]
    #MS2 base peak intensity
    scan_dict["MS2BasePeakInt"]=s["base peak intensity"]
    #name of file :    
    scan_dict["AdjFileName"]=s["spectrum title"][0:24]
    return scan_dict


### Parse MS1 data from mzML for relevant MS1-based annotation and quantification of paired MS1 peptide features that have an overlapping MS1 isotopic profile.

def parse_scan_ms1(s):
    scan_dict_ms1={}
    if s["ms level"] == 2:
        return None
#(Scan Number)
    scan_dict_ms1["ScanNum"]=s["index"]+1 
#MS level
    scan_dict_ms1["MSlevel"]=s["ms level"]
#Array of m/z's
    scan_dict_ms1["m/zArray"]=s["m/z array"]
#Array of respect m/z intensities
    scan_dict_ms1["IntensityArray"]=s["intensity array"]
#MS2 base peak
    scan_dict_ms1["MS2BasePeak"]=s["base peak m/z"]
#MS2 base peak intensity
    scan_dict_ms1["MS2BasePeakInt"]=s["base peak intensity"]
#name of file :    
    scan_dict_ms1["AdjFileName"]=s["spectrum title"][0:24]
#retention time for the scan
    scan_dict_ms1["RetentionTime"]=s['scanList']['scan'][0]["scan start time"]
    return scan_dict_ms1

# In[3]: Function to import mzML via Pyteomics and iterate through each scan to extract relevant MS/MS features from parse_scan function above. Place relevant spectral information in a resultant dataframe.


def get_mzml(mzML_file):
    ## Read in MS/MS data from mzML file
    spectra_reader = mzml.MzML(mzML_file)
    ## Generate list for parsed MS/MS features
    scan_list = []
    ## Go through each scan from mzML and extract MS/MS features only for downstream annotation
    for scan in spectra_reader:
        scan_list.append(parse_scan(scan))
    ## Remove MS1 scan information
    scan_list = [s for s in scan_list if s is not None]
    ## Generate dataframe for resultant MS/MS spectral information
    data = pd.DataFrame.from_records(scan_list)
    ## Return MS/MS features
    return(data)
    
# In[4]: Function to import mzML via Pyteomics and iterate through each scan to extract relevant MS1 scans from parse_scan function above. Place relevant spectral information in a resultant dataframe.

def ms1_get_mzml(mzML_file):
    ## Read in MS1 data from mzML file
    spectra_reader2 = mzml.MzML(mzML_file)
    ## Generate list for parsed MS/MS features
    scan_list2 = []
    ## Go through each scan from mzML and extract MS1 features only for downstream annotation
    for scan in spectra_reader2:
        scan_list2.append(parse_scan_ms1(scan))
    ## Remove MS/MS scan information
    scan_list2 = [s for s in scan_list2 if s is not None]
    ## Generate dataframe for resultant MS1 spectral information
    ms1_spectra = pd.DataFrame.from_records(scan_list2)
    ## Return MS1 features
    return(ms1_spectra)

# In[5]: FDR filtering PSMs at 1% FDR using mokapot. Concatenating all Comet information and mokapot confidence metrics

def get_pin_mokapot_filter(Pin_file):
    import mokapot
    ## Read in .pin file from Comet database search (generally .pin files are filtered for the manuscript beforehand for only correctly coisolated spectra)
    pin = mokapot.read_pin(Pin_file)
    ## Use mokapot to generate FDR-controlled PSMs and resultant SVM model parameters from mokapot
    results,models=mokapot.brew(pin)
    ## Extract the inital data from pin file
    input_data = pin._data
    ## Extract the target PSM results from mokapot with assigned confidence metrics 
    target = results.confidence_estimates["psms"]
    ## Extract the decoy PSM results from mokapot with assigned confidence metrics 
    decoy = results.decoy_confidence_estimates["psms"]
    ## Combine the confidence metrics from mokapot from targets and decoys
    mokapot_df = pd.concat([target,decoy])
    ## Sort mokapot results back in the order of scans again
    mokapot_df.sort_values("ScanNr")
    ## Join/Merge Comet pin information to the PSM results with confidence metrcis from mokapot
    merge_results=pd.merge(input_data, mokapot_df, left_on=['SpecId','Label','ScanNr','ExpMass','CalcMass','Peptide','Proteins'],
    right_on=['SpecId','Label','ScanNr','ExpMass','CalcMass','Peptide','Proteins'],how='left',suffixes=('_in', '_out'))
    ## Filter PSMs at 1% PSM FDR level for downstream analysis
    fdr_filtered_results = merge_results[merge_results['mokapot q-value'] < 0.01]
    ## Print the number of PSMs at 1% FDR following mokapot 
    print("mokapot complete:",fdr_filtered_results.shape[0],"PSMs at 1% FDR")
    return(fdr_filtered_results)


# In[6]: Merge spectral data with the 1% FDR filtered PSMs for downstream annotation and quantification

def merge_results(fdr_filtered_results, data):
    ## Join/Merge 1% FDR filtered PSMs with MS/MS spectral information for annotation and quantification
    quant_input=pd.merge(fdr_filtered_results, data, left_on='ScanNr', right_on='ScanNum', how='left')
    return(quant_input)

#########################           Deconvolution MS/MS-based y-ion quantification of K6:K8 SILAC samples               #########################

# Function for Annotation of b-ions and paired y-ions. Also, includes quantification of paired y-ion features. The method here enables for deconvolution of overlapping quantifications separated by 2Da labels.
## Annotates the b-ion (monoisotopic and first two isotopic errors) and y-ion fragment isotopic profiles which include monoisotopic and four isotope errors within 50 ppm (for charge z1 & z=2 fragments)
## Quantifies features (based on monoisotopic and 4 isotope errors) if at least two isotopes were observed across the 5.
## Calculate Ropt using the Chavez et al. method for deconvoluted SILAC ratio calculation for top most abundant fragment convoluted isotopic ion profiles. 
## Model fits theoretical isotopic distributions based on fragment's atomic composition to observed isotopic profile's for corresponding fragment to determine an Ropt (ratio that minimizes the ratio error between theoretical and observed isotopic distributions
## This method deviates from the Chavez et al implementation for it fits model to each fragment's theoretic distribution (based on the fragment's specific atomic composition). The Chavez et al fits all fragment's to peptide's theoretical distribution (based on peptide's atomic composition)
## Also, filters presented here are different. We accept all Ropt calculations that have two of the observed 5 isotopes and have positive heavy and light contributions for the Ropt calculation. 
## Ropt here is equivalent to 1/Ropt from Chavez because we want ratios to be heavy/light
## Output only a summary file with quant results per PSM hit for top3 and topN 
## Database search based on n-terminal Protein acetylation, methionine oxidation, and Lys8 on lysines as variable modification only in search with the proteome digested with Lys-C. Atomic composition calculation includes oxidation and acetylation additions.
## Adjustments will need to be made for other modifications.

def annotate_fragments(quant_input, Output_name):
    ## generate a number of lists for tracking quants across PSMs
    frag_list= []
    quantifiable_list =[]
    summary_quants=[]
    med_R_opt = []
    med_R_error= []
    med_R_opt3=[]
    med_R_error3=[]
    num_quant_frag = []
    scan_list=[]
    peptide_sequence_list=[]
    ## iterate through each PSM for annotation and quantification of MS/MS features
    for i in range(0,len(quant_input)):
        ## Little timer for the progress in console
        if i % 1000 == 0:
            print(i, "out of",quant_input.shape[0],"PSMs at 1% FDR" )
        elif i == quant_input.shape[0]-1:
            print(i+1, "out of",quant_input.shape[0],"PSMs at 1% FDR" )
        ## extract PSM by row of dataframe
        example=quant_input.iloc[i,:]
        ## Extract scan number
        scan = example['ScanNr']
        ## extract precursor detected m/z
        precursor_mz = example['PrecursorDetected']
        ## Extract PSM precursor charge
        precursor_charge = int(example['Charge'])
        ## Extract all MS/MS spectra information m/s's indexed in ascending order
        mz = example['m/zArray']
        ## Extract all MS/MS spectra information intensities indexed in ascending order matched to m/z array's indicies
        intensity =example['IntensityArray']
        ## Extract peptide amino acid sequence
        peptide=example['Peptide']
        ## Determine the noise level in the MS/MS scan (proxy based on Bakalarski et al. J. Proteome Research 2008)
        noise = median(intensity)
        ## Determine the light peptide sequence
        ### Remove the cleavage ends from the peptide sequence
        original_peptide= peptide[2:-2]
        ## remove the n-terminal (n) annotation from the peptide sequence
        original_peptide=original_peptide.replace("n[42.", "[42.")
        ### Regardless if the heavy or light peptide is triggered we base the deconvolution on the light peptide sequence and then map the whole isotopic profile which contains light and heavy contributions 
        peptide_light=original_peptide.replace("[1.9941]", "")
        ## make a dictionary of all amino acid modifications and add the N-terminal designation for acetylation (looking for that modification individually)
        amino_acid_loc_dict_light={}
        if '[42.0106]' in peptide_light: 
            amino_acid_loc_dict_light['N-term'] = 42.0106
        ## create a list of all amino acids then reverse it for the y-ion series. this contains modification designations as well
        tok_pep_light=re.findall("[A-Z](?:\[[^A-Z]+\])?", peptide_light)
        tok_pep_reverse =  tok_pep_light[::-1]
        ## create a list of just the amino acid sequences parsed by index related to amino acid position and based on single letter amino acid. reverse this list for the y-ion series.
        aa_index_b=re.findall("[A-Z]", peptide_light)
        aa_index_y= aa_index_b[::-1]
        ## map the amino acid delta mass from the modification to dictionary of modifications with the index related to amino acid position from list index
        for ind, aa in enumerate(tok_pep_light):
            if len(aa)>1:
                value=float(re.search("[0-9]+\.[0-9]+",aa).group())
                amino_acid_loc_dict_light[ind] = value
        ## same as above but related to the y-ion series. N-terminal acetylation is ignored in this dictionary because we quantify based on the y-ion series (n-1) fragment ions. 
        amino_acid_loc_dict_light_reverse={} 
        for ind, aa in enumerate(tok_pep_reverse):
            if len(aa)>1:
                value=float(re.search("[0-9]+\.[0-9]+",aa).group())
                amino_acid_loc_dict_light_reverse[ind] = value
        #######################      b-ion theoretical mass annotation        ################################
        ### light peptide monoisotopic mass calculator for the b-ion series
        PepLength=len(tok_pep_light)
        ## initialize the b-ion series based on the proton on the N-terminus of th peptide
        b_fragment = 1.007276
        ### lists for fragment masses, fragment annotation, and whether it is monoisotopic, C13 or 2C13 isotope errors
        ### since there is no deconvultion qunatification calculated for b-ions only the two isotope errors are extracted for b-ions
        masses=[]
        annotation=[]
        isotopes = []
        ### If N-terminal acetylation add the 42 Da mass to all b-ions
        if "N-term" in amino_acid_loc_dict_light:
            b_fragment =b_fragment + amino_acid_loc_dict_light["N-term"]
        ## Walk through amino acid sequence and iteratively calculate the fragment masses for monoisotopic and C13/2C13 errors. Adding on modifications when it applies.
        for i in range(0,PepLength-1):
            ## If their is a modification on the amino acid, calculate the fragment masses
            if i in amino_acid_loc_dict_light:
                ## calculate monoisotopic mass of fragment by adding amino acid mass and the modification delta mass at that amino acid location
                b_fragment =b_fragment + aa_masses_dict[aa_index_b[i]]+amino_acid_loc_dict_light[i]
                ## actual amino acid location is the i index + 1
                index_num = i+1
                ## b-ion masses for charge 1 and charge 2 fragments for monoisotopic mass and two C13 isotope errors
                masses.extend([b_fragment,b_fragment+1.0033548,b_fragment+1.0033548*2,(b_fragment+1.007276)/2,(b_fragment+1.007276+1.0033548)/2,(b_fragment+1.007276+1.0033548*2)/2])
                ## b-ion annotations 
                annotation.extend(["b"+str(index_num)+"+","b"+str(index_num)+"+","b"+str(index_num)+"+","b"+str(index_num)+"++","b"+str(index_num)+"++","b"+str(index_num)+"++"])
                ## b-ion isotope annotations
                isotopes.extend(["M0","1c13","2c13","M0","1c13","2c13" ])
            ## If their is NOT a modification on the amino acid, calculate the fragment masses
            else:
                ## calculate monoisotopic mass of fragment by adding amino acid mass (no amino acid modification to add)
                b_fragment =b_fragment + aa_masses_dict[aa_index_b[i]]
                ## actual amino acid location is the i index + 1
                index_num = i+1
                ## b-ion masses for charge 1 and charge 2 fragments for monoisotopic mass and two C13 isotope errors
                masses.extend([b_fragment,b_fragment+1.0033548,b_fragment+1.0033548*2,(b_fragment+1.007276)/2,(b_fragment+1.007276+1.0033548)/2,(b_fragment+1.007276+1.0033548*2)/2])
                ## b-ion annotations 
                annotation.extend(["b"+str(index_num)+"+","b"+str(index_num)+"+","b"+str(index_num)+"+","b"+str(index_num)+"++","b"+str(index_num)+"++","b"+str(index_num)+"++"])
                ## b-ion isotope annotations
                isotopes.extend(["M0","1c13","2c13","M0","1c13","2c13" ])
        ## Generate dataframe with all theoretical masses for b-ions (only calculate the light version) -- these do not help with quantification so not essential to annotate all heavy b-ion features
        ## also TheoDist was set to NaN because we do not need the b-ion series for deconvolution quantifications (only y-ions apply for this)
        df1 = pd.DataFrame({'TheoMZ':masses,'Annotation':annotation, 'Isotopes':isotopes,"sequence":peptide, "fragmentType" : "b-ion", "HorL" : "both","TheoDist" : 'NaN' })


        #######################      light y-ion theoretical mass annotation        ################################
        ## peptide monoisotopic mass calculator for the y-ion series (determines the fragments monoisotopic mass and first 4 isotope errors (light and heavy up to two isotope errors when delta mass between heavy and light is 2Da)

        PepLength=len(tok_pep_reverse)
        y_fragment = 1.007825 + 1.007825 + 15.994915 + 1.007276
        masses=[]
        annotation=[]
        isotopes = []
        theo_dist_list=[]
        name_Comp=[]
        ## Initialize the composition of the y-ion fragment series
        initialize= [0,2,0,1,0]
        ## Walk down the length of the y-ion series adding amino acid masses and compositions
        for i in range(0,PepLength-1):
            ## if there is a modification on the amino acid, add aa mass + the modification
            if i in amino_acid_loc_dict_light_reverse:
                ## adding the AA mass and the modification mass with the matching peptide index location
                y_fragment =y_fragment + aa_masses_dict[aa_index_y[i]]+amino_acid_loc_dict_light_reverse[i]
                ## actual amino acid location is the i index + 1
                index_num = i+1
                ## Extend the amino acid composition list to the previous y-ion fragment 
                initialize = [a + b for a, b in zip(initialize,Comp_masses_dict[aa_index_y[i]])]
                ## Add modification composition
                if amino_acid_loc_dict_light_reverse[i] == 15.9949:
                    initialize = [a + b for a, b in zip(initialize,[0,0,0,1,0])]
                elif amino_acid_loc_dict_light_reverse[i] == 42.0106:
                    initialize = [a + b for a, b in zip(initialize,[2,2,0,1,0])]
                ## Generate final atom composition formula that is the input to run_calcisotopes (exe that returns the theoretical isotopic distribution for Chavez et al. model fitting.
                composition_form ="C"+str(initialize[0])+"H"+str(initialize[1])+"N"+str(initialize[2])+"O"+str(initialize[3])+"S"+str(initialize[4])
                ## Run exe with atom composition input and out being a list containing the monoisotopic through 4th isotope error for the theoretical isotope profile for Chavez et al. model fit
                theo_dist_value = run_calcisotopes(composition_form)
                ## Append outputs to list (twice for +1 and +2 charge state fragments)
                theo_dist_list.extend(json.loads(theo_dist_value))
                theo_dist_list.extend(json.loads(theo_dist_value))
                ## y-ion masses for charge 1 and charge 2 fragments for monoisotopic mass and four C13 isotope errors
                masses.extend([y_fragment,y_fragment+1.0033548,y_fragment+1.0033548*2,y_fragment+1.0033548*3,y_fragment+1.0033548*4,(y_fragment+1.007276)/2,(y_fragment+1.007276+1.0033548)/2,(y_fragment+1.007276+1.0033548*2)/2,(y_fragment+1.007276+1.0033548*3)/2,(y_fragment+1.007276+1.0033548*4)/2])
                ## y-ion annotations
                annotation.extend(["y"+str(index_num)+"+","y"+str(index_num)+"+","y"+str(index_num)+"+","y"+str(index_num)+"+","y"+str(index_num)+"+","y"+str(index_num)+"++","y"+str(index_num)+"++","y"+str(index_num)+"++","y"+str(index_num)+"++","y"+str(index_num)+"++"])
                ## y-ion isotope annotations
                isotopes.extend(["M0","1c13","2c13","3c13","4c13","M0","1c13","2c13","3c13","4c13"])
            else:
                ## calculate monoisotopic mass of fragment by adding amino acid mass (no amino acid modification to add)
                y_fragment =y_fragment + aa_masses_dict[aa_index_y[i]]
                ## actual amino acid location is the i index + 1
                index_num = i+1
                ## Extend the amino acid composition list to the previous y-ion fragment 
                initialize = [a + b for a, b in zip(initialize,Comp_masses_dict[aa_index_y[i]])]
                ## Generate final atom composition formula that is the input to run_calcisotopes (exe that returns the theoretical isotopic distribution for Chavez et al. model fitting.
                composition_form ="C"+str(initialize[0])+"H"+str(initialize[1])+"N"+str(initialize[2])+"O"+str(initialize[3])+"S"+str(initialize[4])
                ## Run exe with atom composition input and out being a list containing the monoisotopic through 4th isotope error for the theoretical isotope profile for Chavez et al. model fit
                theo_dist_value = run_calcisotopes(composition_form)
                ## Append outputs to list (twice for +1 and +2 charge state fragments)
                theo_dist_list.extend(json.loads(theo_dist_value))
                theo_dist_list.extend(json.loads(theo_dist_value))
                ## y-ion masses for charge 1 and charge 2 fragments for monoisotopic mass and four C13 isotope errors
                masses.extend([y_fragment,y_fragment+1.0033548,y_fragment+1.0033548*2,y_fragment+1.0033548*3,y_fragment+1.0033548*4,(y_fragment+1.007276)/2,(y_fragment+1.007276+1.0033548)/2,(y_fragment+1.007276+1.0033548*2)/2,(y_fragment+1.007276+1.0033548*3)/2,(y_fragment+1.007276+1.0033548*4)/2])
                ## y-ion annotations
                annotation.extend(["y"+str(index_num)+"+","y"+str(index_num)+"+","y"+str(index_num)+"+","y"+str(index_num)+"+","y"+str(index_num)+"+","y"+str(index_num)+"++","y"+str(index_num)+"++","y"+str(index_num)+"++","y"+str(index_num)+"++","y"+str(index_num)+"++"])
                ## y-ion isotope annotations
                isotopes.extend(["M0","1c13","2c13","3c13","4c13","M0","1c13","2c13","3c13","4c13"])
        ## generate a dataframe for the y-ion fragment information including the y-ion theoretical distributions
        df2 = pd.DataFrame({'TheoMZ':masses,'Annotation':annotation,'Isotopes':isotopes,"sequence":peptide, "fragmentType" : "y-ion", "HorL" : "light_heavy" ,"TheoDist" : theo_dist_list})
        ## concatenate the b-ion series and y-ion series dataframes for spectral annotation
        df5 = pd.concat([df1,df2])
        ## sort all observed spectra in ascending oreder for spectral matching
        df6=df5.sort_values(by=['TheoMZ'])

        #########################           Annotation of theoretical masses to observed spectra               #########################
        ### The strategy here is to walk through the theoretical masses from the peptide and map MS/MS features and append intensities with in 50 ppm of theoretical masses
        ### Walk down both lists and track indicies such that both lists only are gone through once 
        
        ## index that maps to observed m/z and intensity arrays
        index_value = 0 
        ## track back index so only go through observed spectra from where last left off from previous annotated peak
        back_index = 0 
        val_options = []
        list_annotation_indicies = []
        list_intensities = []
        ### List of theoretical masses for peptide annotation
        theo_mass =  df6['TheoMZ'].tolist()
        ### For each theoretical fragment mass look for matching observed peak signal within MS/MS ppm tolerance
        for i in range(0,len(theo_mass)):
            ## theoretical m/z mass of fragment to match to
            value = theo_mass[i]
            ## keep looking for observed features in MS/MS if observed signal has mass less than 25 ppm above the theoretical mass and is not the end of the array of observed m/z's
            while mz[index_value] < value + value *0.000025 and index_value < len(mz) -1:
                ## if an observed MS/MS peak maps within the 50ppm tolerance of the theoretical fragment mass
                if mz[index_value] > value - value*0.000025 and mz[index_value] < value + value*0.000025:
                    ## append the options for the matched MS/MS observed peak or peaks that can be assigned to that theoretical mass
                    val_options.append(intensity[index_value])
                    ## Increase the index value 
                    index_value += 1
                elif mz[index_value] < value - value*0.000025:
                    back_index = index_value
                    ## keep moving up the index value until within range
                    index_value += 1
                ## keep moving up index. this enables looking for multiple features until you are outside the 25 ppm tolerance above.
                else: 
                    index_value += 1
            ## If there are multiple observed MS/MS peaks m/z's that fall within the 50 ppm tolerance of the theoretical mass then max intensity feature is choosen
            if len(val_options) >0 :
                ## index location appended 
                list_annotation_indicies.append(i)
                ## add annotation of the theoretical mass with the max intensity of matched MS/MS peak that falls within 50 ppm of the theoretical.
                list_intensities.append(max(val_options))
                ## reset valid options for matched observed to next theoretical mass
                val_options=[]
            elif len(val_options) == 0 :
                list_annotation_indicies.append(i)
                list_intensities.append(0)
            ## initiate new back index
            index_value = back_index
        ## reset indexes of dataframe because not all theoretical features will have an observed MS/MS
        df_final = df6.iloc[list_annotation_indicies].reset_index()
        ## append the mapped intensities for each annotation with the same indicies applied to ensure accurate mapping of correct features
        df_final['intensity'] = list_intensities

        #########################           Deconvolution of K6:K8 SILAC ratios               #########################
        ### The method is as outlined in the manuscript and the Chavez et al (Analytical Chemistry 2020)
        ### Here we take the annotated observed spectral profile for each fragment and fit it to the theoretical isotope profile for said fragment 
        ### We extract a Ropt value, which is the value that minimizes the ratio_error between the observed and theoretical isotope profiles (as defined by Chavez et al.)
        ### Ropt values are determined for the top3 and topN most abundant y-ion fragments with corresponding ratio error's
        
        ### extract y-ion from b-ion information for observed spectra
        light_df = df_final[df_final.HorL  == "light_heavy"]
        bion_df = df_final[df_final.HorL  == "both"]       
        unique = []
        ## extract a list of the unique annotations
        [unique.append(x) for x in light_df['Annotation'].values.tolist() if x not in unique]
        R_opt_vals = []
        R_error_vals = []
        fragment_total = []
        quant_frag=0
        ### Go through each y-ion fragment based on their annotation and subset the dataframe to perform the K6:K8 deconvolution quantification
        for i in unique:
            ### subset dataframe for a given y-ion fragment (i)
            test=light_df.loc[light_df['Annotation'] == i]
            ### Calculate summed intensity for the theoretical distribtuion and the observed distribution to normalize the readouts for comparsion via the calculation of xi and obs
            total = sum(test['TheoDist'])
            obs_total = sum(test['intensity'])
            ### Determine the number of isotopes above the intensity of 0 because we can use this value above 2 required for quantification filter.
            num_isotope_above0 =sum(i>0 for i in test['intensity'].values.tolist())
            ### If there is observed signal for the fragment across any of the isotopes execute the model by Chavez et al.
            if obs_total>0:
                ### Normalized theoretical distribution
                xi = [number / total for number in test['TheoDist'].values.tolist()]
                ### Normalized observed distribution
                obs = [number / obs_total for number in test['intensity'].values.tolist()]
                ### summed theoretical distribution across monoisotopic and the first two isotopes (which are present in the light and heavy forms when looking at the first 5 isotopes)
                z= sum(xi[0:3])
                omega = [z*obs[0],z*obs[1],z*obs[2]-xi[0],z*obs[3]-xi[1],z*obs[4]-xi[2] ]
                s_val= [xi[0] -obs[0],xi[1] -obs[1],xi[2] -obs[2],xi[3] -obs[3],xi[4] -obs[4]]
                a_val = sum(map(lambda x:x*x,omega))
                b_val = sum([a * b for a, b in zip(omega, s_val)])
                c_val = sum(map(lambda x:x*x,s_val))
                ### Equation for calculating the optimal ratio (Chavez et al). Here we use heavy/light so the numerator and the denominator are flipped.
                R_opt = (z*c_val + b_val) / (a_val + z*b_val)
                ### This is the R_opt light/heavy that we can use fo calculating the ratio error
                R_opt_original = 1/R_opt
                ### y-ion fragment ratio error as defined by Chavez et al. (Analytical Chemistry 2020)
                error_R = (c_val*R_opt_original**2 - 2*b_val*R_opt_original + a_val)/((z+R_opt_original)**2)
                ### y-ion deconvolution quantification filter requiring a positive heavy and light contribution as well as at least two observed isotopes that have intensities above 0. Then can append values
                if (z*c_val + b_val) >0 and (a_val + z*b_val) >0 and num_isotope_above0 > 1:
                    ### append R_opt quant if meet above criteria (same for error_Ropt and total quant observed for ordering topN)
                    ### keep track of number of quantified fragments for a given PSM
                    R_opt_vals.append(R_opt)
                    R_error_vals.append(error_R)
                    quant_frag +=1
                    fragment_total.append(obs_total)
        ### If there are y-ion fragment based quantifications calculate topN and top3
        if len(R_opt_vals) >0: 
            ### TopN quantifications for the median Ropt and ratio error
            med_R_opt.append(median(R_opt_vals))            
            med_R_error.append(median(R_error_vals))  
            num_quant_frag.append(median(quant_frag)) 
            ### Top3 quantifications for the median Ropt and ratio error
            if len(R_opt_vals)>2:
                sort_index = np.argsort(np.array(fragment_total))[::-1][:3]
                R_opt3 = median(np.array(R_opt_vals)[sort_index])
                R_error3 = median(np.array(R_error_vals)[sort_index])
                med_R_opt3.append(R_opt3)
                med_R_error3.append(R_error3)
            ### If no top3 but have topN
            else:
                med_R_opt3.append("NaN")
                med_R_error3.append("NaN")
        ### If no topN or top3 quants: put NaN
        else: 
            med_R_opt.append("NaN")            
            med_R_error.append("NaN")  
            num_quant_frag.append(0) 
            med_R_opt3.append("NaN")
            med_R_error3.append("NaN") 
        peptide_sequence_list.append(original_peptide)
        scan_list.append(scan)
    ### Generate all quantifications as a dataframe
    quant_df = pd.DataFrame({'scan':scan_list,'peptide_seq':peptide_sequence_list,'median_Ropt_topN':med_R_opt,'median_Rerror_topN':med_R_error,'median_Ropt_top3':med_R_opt3,"median_Rerror_top3":med_R_error3, "num_quant_fragments" : num_quant_frag})
    ### Generate a dataframe that removes the spectral information of m/z's and intensities
    quant_input3 = quant_input
    del quant_input3['m/zArray']
    del quant_input3['IntensityArray']    
    ### Merge PSM identification results to the MS/MS-based K6:K8 deconvolution quantification. Merged based on the scan number 
    summary= pd.merge(quant_input3, quant_df, left_on="ScanNr",right_on="scan",how="outer")
    return(summary)

#########################           Deconvolution MS/MS-based y-ion quantification of K6:K8 SILAC samples               #########################

# Function of MS1 peptide pair features. The method here enables for deconvolution of overlapping quantifications separated by 2Da labels.
## Annotates the precursor pair's isotopic profiles which include monoisotopic and four isotope errors within 50 ppm (for charge z1 & z=2 fragments)
## Quantifies features (based on monoisotopic and 4 isotope errors) if at least two isotopes were observed across the 5.
## Calculate Ropt using the Chavez et al. method for deconvoluted SILAC ratio calculation for precursor MS1 scans across the MS1 elution time. 
## Model fits theoretical isotopic distributions based on fragment's atomic composition to observed isotopic profile's for corresponding fragment to determine an Ropt (ratio that minimizes the ratio error between theoretical and observed isotopic distributions)
## This method from the Chavez et al implementation has not been used for MS1 quantificaiton (based on peptide's atomic composition). This is the first instance of this model applied to MS1 features.
## Also, filters presented here are different. We accept all Ropt calculations that have two of the observed 5 isotopes and have positive heavy and light contributions for the Ropt calculation. 
## Ropt here is equivalent to 1/Ropt from Chavez because we want ratios to be heavy/light
## Output only a summary file with quant results per PSM hit for top3 and top5 most abundant successive scans (median based), also calculate a R_opt based on the Ms1 within 16 scan of PSM with max signal across isotopes and the MS1 scan nearest to the PSM scan.
## Database search based on n-terminal Protein acetylation, methionine oxidation, and Lys8 on lysines as variable modification (Lys6 static mod) only in search with the proteome digested with Lys-C. Atomic composition calculation includes oxidation and acetylation additions.



def ms1_deconv_quant(fdr_filtered_results, spectra_ms1):
    R_opt_vals_apex = []
    R_error_vals_apex = []
    R_opt_vals_nearest=[]
    R_error_vals_nearest=[]
    R_opt_vals_weight = []
    R_error_vals_weight = []
    R_opt_vals_weight5 = []
    R_error_vals_weight5 = []
    scan_list_values=[]
    med_R_opt=[]
    med_R_error=[]
    num_quant_frag=[]
    med_R_opt5=[]
    med_R_error5=[]
    num_quant_frag5=[]
    ## extract the MS1 features for all the PSMs only if they meet the same criteria for quantification filter as is by fragments at the MS/MS level
    for i in range(0,len(fdr_filtered_results)):
        ## Output counter for MS1 quantifications
        if i % 1000 == 0:
            print(i, "out of",fdr_filtered_results.shape[0],"PSMs at 1% FDR" )
        elif i == fdr_filtered_results.shape[0]-1:
            print(i+1, "out of",fdr_filtered_results.shape[0],"PSMs at 1% FDR" )
        ### Go through 1 PSM at a time
        example=fdr_filtered_results.iloc[i,:]
        scan_num_val=example["ScanNr"]
        scan_list_values.append(scan_num_val)
        ### Determine the charge state of the PSM that we are trying to quantify from Pin file results
        if example["Charge1"] ==1:
            psm_charge = 1
        elif example["Charge2"] ==1:
            psm_charge = 2
        elif example["Charge3"] ==1:
            psm_charge = 3
        elif example["Charge4"] ==1:
            psm_charge = 4
        elif example["Charge5"] ==1:
            psm_charge = 5
        else:
            psm_charge = 6

        # extract the MS1 data from the nearest 16 scans to the PSM. By increasing this size it can really slow things down.  16 scans at a standard duty cycle (2-3 seconds) should capture standard peak widths 30 seconds. Can be altered
        df_sort = spectra_ms1.iloc[(spectra_ms1['ScanNum']-example["ScanNr"]).abs().argsort()[:16]]
        df_sort2 = df_sort.sort_index()
        ## Determine the nearest MS1 scan to the PSM scan number
        df_nearest = spectra_ms1.iloc[(spectra_ms1['ScanNum']-example["ScanNr"]).abs().argsort()[:1]]
        nearest_value = df_nearest.iloc[0]['ScanNum']
        ### determine the light precursor peptide sequence to enable peptide atomic composition calculation for theoretical isotope calculation
        peptide=example['Peptide']
        original_peptide= peptide[2:-2]
        ## Determine number of heavy lysines and N-terminal acetylation and oxidation for atomic composition calculation
        NumOx = original_peptide.count( "[15.9949]" ) 
        NumAc = original_peptide.count( "n[42.0106]" )
        NumK_H = original_peptide.count( "[1.9941]" )
        ### Remove the modifications to generate a bare sequence. Not applicable for all modes. Could be altered to make it for all peptide sequences.
        original_peptide=original_peptide.replace("n[42.0106]", "")
        original_peptide=original_peptide.replace("[15.9949]", "")
        peptide_light=original_peptide.replace("[1.9941]", "")
        ## generate a peptide sequence list
        tok_pep_light=re.findall("[A-Z](?:\[[^A-Z]+\])?", peptide_light)

        ### Initialize the whole peptide (added the initialized counts for the b-ion and y-ion )
        initialize= [0,3,0,1,0]
        ## Based on number of modifications in the peptide (nAc and mOx) add those atomic contributions 
        if NumOx > 0:
            vec = [x * NumOx for x in [0,0,0,1,0]]
            initialize = [a + b for a, b in zip(initialize,vec)]
        elif NumAc ==1 :
            initialize = [a + b for a, b in zip(initialize,[2,2,0,1,0])]

        ### Add up the atomic compositions for the entire peptide sequence
        for i in tok_pep_light:
            initialize = [a + b for a, b in zip(initialize,Comp_masses_dict[i])]
        ### Generate the input to the exe for the calculating the theoretical isotope distribtuion for the peptide precursor
        composition_form ="C"+str(initialize[0])+"H"+str(initialize[1])+"N"+str(initialize[2])+"O"+str(initialize[3])+"S"+str(initialize[4])    
        ## Run exe with atom composition input and out being a list containing the monoisotopic through 4th isotope error for the theoretical isotope profile for Chavez et al. model fit
        theo_dist_value = run_calcisotopes(composition_form)
        ## convert from bit based to values
        peptide_theo_dist = json.loads(theo_dist_value)
        ### Determine the precursor m/z's to match
        theo_mz = example["CalcMass"] / psm_charge + (1.007276 * (psm_charge-1 ))/psm_charge - (1.9941 * NumK_H)/psm_charge
        ### Append the peptide's PSM monositopic through 4th isotope error 
        masses_theo = [theo_mz,theo_mz+1.0033548/psm_charge,theo_mz+(1.0033548*2)/psm_charge,theo_mz+(1.0033548*3)/psm_charge,(theo_mz+(1.0033548*4)/psm_charge)]

        ### Annotation of the isotopic peaks across all 16 scans

        all_matched_intensity = []
        isotope_sum_intensity = []
        scan_num_list=[]

        ### go through each MS1 scan and extract the isotope observed intensity for the PSM's precursor
        ### this is the same as MS/MS script for matching fragment ions but here with the same precursor masses across multiple MS1 scans
        for j in range(0,len(df_sort2)):
            ## Extract MS1 intensity and m/z (both with matched indicies)
            mz = df_sort2.iloc[j,:]["m/zArray"]
            intensity = df_sort2.iloc[j,:]["IntensityArray"]
            ## Extract the MS1 scan number
            scan_num_forList = df_sort2.iloc[j,:]["ScanNum"]
            index_value = 0 
            back_index = 0 
            val_options = []
            list_annotation_indicies = []
            list_intensities = []
            list_obs_mz=[]
            obs_values_mz=[]
            ## now match MS1 scan to theoretical calculated precursor masses
            for i in range(0,len(masses_theo)):
                value = masses_theo[i]
                while mz[index_value] < value + value *0.000025 and index_value < len(mz) -1:
                    if mz[index_value] > value - value*0.000025 and mz[index_value] < value + value*0.000025:
                        val_options.append(intensity[index_value])
                        obs_values_mz.append(mz[index_value])
                        index_value += 1
                    elif mz[index_value] < value - value*0.000025:
                        back_index = index_value
                        index_value += 1
                    else: 
                        index_value += 1
                if len(val_options) >0 :
                    list_annotation_indicies.append(i)
                    ### Extract the max intensity matched MS1 feature near isotope mass
                    list_intensities.append(max(val_options))
                    list_obs_mz.append(max(obs_values_mz))
                    val_options=[]
                    obs_values_mz=[]
                elif len(val_options) == 0 :
                    list_annotation_indicies.append(i)
                    list_intensities.append(0)
                    list_obs_mz.append(value)
                index_value = back_index
            all_matched_intensity.append(list_intensities)
            sum_val = sum(list_intensities)
            isotope_sum_intensity.append(sum_val)
            scan_num_list.append(scan_num_forList)
        
        ###determine index of the nearest scan 
        counter = 0
        for num in scan_num_list:
            if num == nearest_value:
                index_val_scan = counter
            counter+=1
            
        ### determine the scan with the max summed intensity
        max_scan_ms1 = all_matched_intensity[isotope_sum_intensity.index(max(isotope_sum_intensity))]
        ### calculate the indicies with the max sum across three consequetive MS1 scans (top3 median)
        list_3sums = []
        for i in range(0,len(isotope_sum_intensity)-2):
            value_sum = isotope_sum_intensity[i] + isotope_sum_intensity[i+1] +isotope_sum_intensity[i+2]
            list_3sums.append(value_sum)
        max_index_value =list_3sums.index(max(list_3sums))
        top_triplet = all_matched_intensity[max_index_value:max_index_value+3]
        
        ###  calculate the indicies with the max sum across three consequetive MS1 scans (top5 median)
        list_5sums = []
        for i in range(0,len(isotope_sum_intensity)-4):
            value_sum = isotope_sum_intensity[i] + isotope_sum_intensity[i+1] +isotope_sum_intensity[i+2]+ isotope_sum_intensity[i+3] +isotope_sum_intensity[i+4]
            list_5sums.append(value_sum)
        max_index_value5 =list_5sums.index(max(list_5sums))
        top_5plex = all_matched_intensity[max_index_value5:max_index_value5+5]

        ## Calculate the Ropt and ratio error for max intensity scan for PSM
        ### Same calculation as MS/MS
        total = sum(peptide_theo_dist)
        obs_total = sum(max_scan_ms1)
        num_isotope_above0 = sum(x>0 for x in max_scan_ms1)
        if obs_total>0:
            xi = [number / total for number in peptide_theo_dist]
            obs = [number / obs_total for number in max_scan_ms1]
            z= sum(xi[0:3])
            omega = [z*obs[0],z*obs[1],z*obs[2]-xi[0],z*obs[3]-xi[1],z*obs[4]-xi[2] ]
            s_val= [xi[0] -obs[0],xi[1] -obs[1],xi[2] -obs[2],xi[3] -obs[3],xi[4] -obs[4]]
            a_val = sum(map(lambda x:x*x,omega))
            b_val = sum([a * b for a, b in zip(omega, s_val)])
            c_val = sum(map(lambda x:x*x,s_val))
            R_opt = (z*c_val + b_val) / (a_val + z*b_val)
            R_opt_original = 1/R_opt
            error_R = (c_val*R_opt_original**2 - 2*b_val*R_opt_original + a_val)/((z+R_opt_original)**2)
            if (z*c_val + b_val) >0 and (a_val + z*b_val) >0 and num_isotope_above0>1:
                R_opt_vals_apex.append(R_opt)
                R_error_vals_apex.append(error_R)
            else: 
                R_opt_vals_apex.append("NaN")            
                R_error_vals_apex.append("NaN") 
        else:
            R_opt_vals_apex.append("NaN")            
            R_error_vals_apex.append("NaN")         

        ### Calculation for the nearest MS1 scan to PSM scan
        nearest_ms1_scan_obs = all_matched_intensity[index_val_scan]
        total = sum(peptide_theo_dist)
        obs_total = sum(nearest_ms1_scan_obs)
        num_isotope_above0 = sum(x>0 for x in nearest_ms1_scan_obs)
        if obs_total>0:
            xi = [number / total for number in peptide_theo_dist]
            obs = [number / obs_total for number in nearest_ms1_scan_obs]
            z= sum(xi[0:3])
            omega = [z*obs[0],z*obs[1],z*obs[2]-xi[0],z*obs[3]-xi[1],z*obs[4]-xi[2] ]
            s_val= [xi[0] -obs[0],xi[1] -obs[1],xi[2] -obs[2],xi[3] -obs[3],xi[4] -obs[4]]
            a_val = sum(map(lambda x:x*x,omega))
            b_val = sum([a * b for a, b in zip(omega, s_val)])
            c_val = sum(map(lambda x:x*x,s_val))
            R_opt = (z*c_val + b_val) / (a_val + z*b_val)
            R_opt_original = 1/R_opt
            error_R = (c_val*R_opt_original**2 - 2*b_val*R_opt_original + a_val)/((z+R_opt_original)**2)
            if (z*c_val + b_val) >0 and (a_val + z*b_val) >0 and num_isotope_above0>1:
                R_opt_vals_nearest.append(R_opt)
                R_error_vals_nearest.append(error_R)
            else: 
                R_opt_vals_nearest.append("NaN")            
                R_error_vals_nearest.append("NaN") 
        else:
            R_opt_vals_nearest.append("NaN")            
            R_error_vals_nearest.append("NaN")               
                
                
        ### Weighted average quantification for the top3 consecutive scans 
        weighted_top3 = [a + b + c for a, b,c in zip(top_triplet[0], top_triplet[1], top_triplet[2])]       
        total = sum(peptide_theo_dist)
        obs_total = sum(weighted_top3)
        num_isotope_above0 = sum(x>0 for x in weighted_top3)
        if obs_total>0:
            xi = [number / total for number in peptide_theo_dist]
            obs = [number / obs_total for number in weighted_top3]
            z= sum(xi[0:3])
            omega = [z*obs[0],z*obs[1],z*obs[2]-xi[0],z*obs[3]-xi[1],z*obs[4]-xi[2] ]
            s_val= [xi[0] -obs[0],xi[1] -obs[1],xi[2] -obs[2],xi[3] -obs[3],xi[4] -obs[4]]
            a_val = sum(map(lambda x:x*x,omega))
            b_val = sum([a * b for a, b in zip(omega, s_val)])
            c_val = sum(map(lambda x:x*x,s_val))
            R_opt = (z*c_val + b_val) / (a_val + z*b_val)
            R_opt_original = 1/R_opt
            error_R = (c_val*R_opt_original**2 - 2*b_val*R_opt_original + a_val)/((z+R_opt_original)**2)
            if (z*c_val + b_val) >0 and (a_val + z*b_val) >0 and num_isotope_above0>1:
                R_opt_vals_weight.append(R_opt)
                R_error_vals_weight.append(error_R)
            else: 
                R_opt_vals_weight.append("NaN")            
                R_error_vals_weight.append("NaN") 
        else:
            R_opt_vals_weight.append("NaN")            
            R_error_vals_weight.append("NaN") 
                
        ### Weighted average quantification for the top5 consecutive scans 
        weighted_top5 = [a + b + c+d+e for a, b,c,d,e in zip(top_5plex[0], top_5plex[1], top_5plex[2], top_5plex[3], top_5plex[4])]       
        total = sum(peptide_theo_dist)
        obs_total = sum(weighted_top5)
        num_isotope_above0 = sum(x>0 for x in weighted_top5)
        if obs_total>0:
            xi = [number / total for number in peptide_theo_dist]
            obs = [number / obs_total for number in weighted_top5]
            z= sum(xi[0:3])
            omega = [z*obs[0],z*obs[1],z*obs[2]-xi[0],z*obs[3]-xi[1],z*obs[4]-xi[2] ]
            s_val= [xi[0] -obs[0],xi[1] -obs[1],xi[2] -obs[2],xi[3] -obs[3],xi[4] -obs[4]]
            a_val = sum(map(lambda x:x*x,omega))
            b_val = sum([a * b for a, b in zip(omega, s_val)])
            c_val = sum(map(lambda x:x*x,s_val))
            R_opt = (z*c_val + b_val) / (a_val + z*b_val)
            R_opt_original = 1/R_opt
            error_R = (c_val*R_opt_original**2 - 2*b_val*R_opt_original + a_val)/((z+R_opt_original)**2)
            if (z*c_val + b_val) >0 and (a_val + z*b_val) >0 and num_isotope_above0>1:
                R_opt_vals_weight5.append(R_opt)
                R_error_vals_weight5.append(error_R)
            else: 
                R_opt_vals_weight5.append("NaN")            
                R_error_vals_weight5.append("NaN") 
        else:
            R_opt_vals_weight5.append("NaN")            
            R_error_vals_weight5.append("NaN") 
        
        ### Median quantification for the top3 consecutive scans 
        R_opt_vals = []
        R_error_vals = []
        fragment_total = []
        annotation_list=[]
        quant_frag=0
        for i in top_triplet:
            total = sum(peptide_theo_dist)
            obs_total = sum(i)
            num_isotope_above0 = sum(x>0 for x in i)
            if obs_total>0:
                xi = [number / total for number in peptide_theo_dist]
                obs = [number / obs_total for number in i]
                z= sum(xi[0:3])
                omega = [z*obs[0],z*obs[1],z*obs[2]-xi[0],z*obs[3]-xi[1],z*obs[4]-xi[2] ]
                s_val= [xi[0] -obs[0],xi[1] -obs[1],xi[2] -obs[2],xi[3] -obs[3],xi[4] -obs[4]]
                a_val = sum(map(lambda x:x*x,omega))
                b_val = sum([a * b for a, b in zip(omega, s_val)])
                c_val = sum(map(lambda x:x*x,s_val))
                R_opt = (z*c_val + b_val) / (a_val + z*b_val)
                R_opt_original = 1/R_opt
                error_R = (c_val*R_opt_original**2 - 2*b_val*R_opt_original + a_val)/((z+R_opt_original)**2)
                if (z*c_val + b_val) >0 and (a_val + z*b_val) >0 and num_isotope_above0>1:
                    R_opt_vals.append(R_opt)
                    R_error_vals.append(error_R)
                    annotation_list.append(i)
                    quant_frag +=1
                    fragment_total.append(obs_total)
        if len(R_opt_vals) >0: 
            med_R_opt.append(median(R_opt_vals))            
            med_R_error.append(median(R_error_vals))  
            num_quant_frag.append(median(quant_frag)) 
        else: 
            med_R_opt.append("NaN")            
            med_R_error.append("NaN")  
            num_quant_frag.append(0) 
            
        ### Median quantification for the top3 consecutive scans 
        R_opt_vals5 = []
        R_error_vals5 = []
        fragment_total5 = []
        annotation_list5=[]
        quant_frag5=0
        for i in top_5plex:
            total = sum(peptide_theo_dist)
            obs_total = sum(i)
            num_isotope_above0 = sum(x>0 for x in i)
            if obs_total>0:
                xi = [number / total for number in peptide_theo_dist]
                obs = [number / obs_total for number in i]
                z= sum(xi[0:3])
                omega = [z*obs[0],z*obs[1],z*obs[2]-xi[0],z*obs[3]-xi[1],z*obs[4]-xi[2] ]
                s_val= [xi[0] -obs[0],xi[1] -obs[1],xi[2] -obs[2],xi[3] -obs[3],xi[4] -obs[4]]
                a_val = sum(map(lambda x:x*x,omega))
                b_val = sum([a * b for a, b in zip(omega, s_val)])
                c_val = sum(map(lambda x:x*x,s_val))
                R_opt = (z*c_val + b_val) / (a_val + z*b_val)
                R_opt_original = 1/R_opt
                error_R = (c_val*R_opt_original**2 - 2*b_val*R_opt_original + a_val)/((z+R_opt_original)**2)
                if (z*c_val + b_val) >0 and (a_val + z*b_val) >0 and num_isotope_above0>1:
                    R_opt_vals5.append(R_opt)
                    R_error_vals5.append(error_R)
                    annotation_list5.append(i)
                    quant_frag +=1
                    fragment_total5.append(obs_total)
        if len(R_opt_vals5) >0: 
            med_R_opt5.append(median(R_opt_vals5))            
            med_R_error5.append(median(R_error_vals5))  
            num_quant_frag5.append(median(quant_frag5)) 
        else: 
            med_R_opt5.append("NaN")            
            med_R_error5.append("NaN")  
            num_quant_frag5.append(0) 
    ## Dataframe for all of the MS1 quantifications for the PSM level which can be merged to all other data (PSM info and MS/MS-based quantifications)
    ms1_quant_df = pd.DataFrame({'scan':scan_list_values,'ms1_apex_Ropt':R_opt_vals_apex,'ms1_apex_Rerror':R_error_vals_apex,'ms1_nearest_Ropt':R_opt_vals_nearest,'ms1_nearest_Rerror':R_error_vals_nearest,'sum_ms1_top3_Ropt':R_opt_vals_weight,'sum_ms1_top3_Rerror':R_error_vals_weight,'median_ms1_top3_Ropt':med_R_opt,"median_ms1_top3_Rerror":med_R_error,'sum_ms1_top5_Ropt':R_opt_vals_weight5,'sum_ms1_top5_Rerror':R_error_vals_weight5,'median_ms1_top5_Ropt':med_R_opt5,"median_ms1_top5_Rerror":med_R_error5})
    return(ms1_quant_df)


### Merge the PSM info / MS/MS-based quant dataframe to the dataframe for MS1-based quantification via the scan number for the PSM

def merge_ms1(ms1_quant_df, summary_df):
    final_summary_df=pd.merge(summary_df, ms1_quant_df, left_on='scan', right_on='scan', how='left')
    return(final_summary_df)


###Main function

def main(mzML_file,Pin_file,Output_name):
    start_time = datetime.now()
    spectra = get_mzml(mzML_file)
    print("mzml import - ms/ms")
    spectra_ms1 = ms1_get_mzml(mzML_file)
    print("mzml import - ms1")
    print("Comet pin import")
    fdr_filtered_results = get_pin_mokapot_filter(Pin_file)
    merge_df = merge_results(fdr_filtered_results,spectra)
    print("merge success")
    summary_df = annotate_fragments(merge_df, Output_name)
    print("annotate success")
    ms1_quant_df = ms1_deconv_quant(fdr_filtered_results, spectra_ms1)
    print("ms1 Dinosaur feature map success")
    final_summary_df = merge_ms1(ms1_quant_df,summary_df)
    print("merge ms1 to summary quants")
    final_summary_df.to_csv(Output_name, sep=',') 
    print("export success")  
    end_time = datetime.now()
    print('Duration: {}'.format(end_time - start_time))
    
    
if __name__ == "__main__":
    ###Run command
    mzML_file = sys.argv[1]
    Pin_file = sys.argv[2]
    Output_name = sys.argv[3]
    main(mzML_file,Pin_file,Output_name)

#python Coiso_K6K8.py "filename.mzML" "filename.pin" "filename_MS1_MS2_quant.csv"

#!/usr/bin/env python
import argparse
import os 
import sys
from .Coiso_K0K8 import main as K0K8_main
from .Coiso_K6K8 import main as K6K8_main

#to move to Coiso_K6K8


def main():
    parser = argparse.ArgumentParser(description="Coiso SILAC Quantification Tool", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-q", "--quant_type", required=True, help="Designate K0K8 or K6K8 SILAC labels used")
    parser.add_argument("-m", "--mzML_file",required=True,  help=".mzML file for spectral processing")
    parser.add_argument("-p", "--pin_file",required=True,  help="Coiso Comet .pin filename")
    parser.add_argument("-d", "--dinosaur_file",  help="Dinosaur .feature.tsv filename - K0K8 only")
    parser.add_argument("-o", "--output_name",required=True,  help="Quant output filename : name as .csv")
    args = parser.parse_args()
    
    if args.quant_type == "K0K8":
        if args.dinosaur_file is None:
            parser.error("K0K8 requires dinosaur_file (-d). See help for more info.")
            
        print("K0K8 Coiso SILAC Analysis Start")
        K0K8_main(args.mzML_file, args.pin_file, args.dinosaur_file, args.output_name)
        print("K0K8 Coiso SILAC Analysis Complete")
        
    elif args.quant_type == "K6K8":
        print("K6K8 Coiso SILAC Analysis Start")
        K6K8_main(args.mzML_file, args.pin_file, args.output_name)
        print("K6K8 Coiso SILAC Analysis Complete")
        
    else:
        print("please run python __main__.py -h")
        
if __name__ == "__main__":
    main()
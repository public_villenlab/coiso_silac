#!/usr/bin/env python
# coding: utf-8

# In[1]: Import packages necessary for script

import sys
from pyteomics import mzml   
import pandas as pd
import re
import numpy as np
from pyteomics import mgf
import mokapot
from re import search
from numpy import median
from datetime import datetime
from sklearn.linear_model import LinearRegression

# In[2]: Define function for parsing the MS/MS data for annotation

### Dictionary of the monoisotopic masses for K0 (light sample) with IAA alkylated cysteines as a static modification
aa_masses_dict = {'G': 57.021463735,
'A': 71.037113805,
'S': 87.032028435,
'P': 97.052763875,
'V': 99.068413945,
'T': 101.047678505,
'C': 160.030648505,## with c+57  103.009184505 + 57.021464  160.030648505
'L': 113.084064015,
'I': 113.084064015,
'N': 114.04292747,
'D': 115.026943065,
'Q': 128.05857754,
'K': 128.09496305,
'E': 129.042593135,
'M': 131.040484645,
'H': 137.058911875,
'F': 147.068413945,
'R': 156.10111105,
'Y': 163.063328575,
'W': 186.07931298}

### Parse MS/MS data from mzML for relevant MS/MS-based annotation and quantification of paired y-ion features and b-ion features
def parse_scan(s):
    scan_dict={}
    # Only extracting MS/MS data: removing the MS1 scans
    if s["ms level"] == 1:
        return None
    # Scan Number
    scan_dict["ScanNum"]=s["index"]+1 
    # MS level
    scan_dict["MSlevel"]=s["ms level"]
    #(type of scan, selected ion location (not original selected precursor), CE, and window size) 
    scan_dict["ScanInfo"]=s["scanList"]["scan"][0]["filter string"]  
    ## retention time
    scan_dict["RetentionTime"]=s["scanList"]["scan"][0]["scan start time"]
    # Precursor detected that is triggering the offset from the MS1
    scan_dict["PrecursorDetected"]=s["precursorList"]["precursor"][0]["selectedIonList"]["selectedIon"][0]["selected ion m/z"]
    # Center of isolation window target
    scan_dict["MS2TargetMass"]=s["precursorList"]["precursor"][0]["isolationWindow"]["isolation window target m/z"]
    # Isolation window size
    scan_dict["IsolationSize"]=s["precursorList"]["precursor"][0]["isolationWindow"]["isolation window lower offset"]*2
    # charge state
    scan_dict["Charge"]=s["precursorList"]["precursor"][0]["selectedIonList"]["selectedIon"][0]["charge state"] 
    # Array of m/z's
    scan_dict["m/zArray"]=s["m/z array"]
    # Array of respect m/z intensities
    scan_dict["IntensityArray"]=s["intensity array"]
    # MS2 base peak
    scan_dict["MS2BasePeak"]=s["base peak m/z"]
    # MS2 base peak intensity
    scan_dict["MS2BasePeakInt"]=s["base peak intensity"]
    # name of file :    
    scan_dict["AdjFileName"]=s["spectrum title"][0:24]
    # return data
    return scan_dict


# In[3]: Function to import mzML via Pyteomics and iterate through each scan to extract relevant MS/MS features from parse_scan function above. Place relevant spectral information in a resultant dataframe.

def get_mzml(mzML_file):
    ## Read in MS/MS data from mzML file
    spectra_reader = mzml.MzML(mzML_file)
    ## Generate list for parsed MS/MS features
    scan_list = []
    ## Go through each scan from mzML and extract MS/MS features only for downstream annotation
    for scan in spectra_reader:
        scan_list.append(parse_scan(scan))
    ## Remove MS1 scan information
    scan_list = [s for s in scan_list if s is not None]
    ## Generate dataframe for resultant MS/MS spectral information
    data = pd.DataFrame.from_records(scan_list)
    ## Return MS/MS features
    return(data)

# In[5]: FDR filtering PSMs at 1% FDR using mokapot. Concatenating all Comet information and mokapot confidence metrics

def get_pin_mokapot_filter(Pin_file):
    import mokapot
    ## Read in .pin file from Comet database search (generally .pin files are filtered for the manuscript beforehand for only correctly coisolated spectra)
    pin = mokapot.read_pin(Pin_file)
    ## Use mokapot to generate FDR-controlled PSMs and resultant SVM model parameters from mokapot
    results,models=mokapot.brew(pin)
    ## Extract the inital data from pin file
    input_data = pin._data
    ## Extract the target PSM results from mokapot with assigned confidence metrics 
    target = results.confidence_estimates["psms"]
    ## Extract the decoy PSM results from mokapot with assigned confidence metrics 
    decoy = results.decoy_confidence_estimates["psms"]
    ## Combine the confidence metrics from mokapot from targets and decoys
    mokapot_df = pd.concat([target,decoy])
    ## Sort mokapot results back in the order of scans again
    mokapot_df.sort_values("ScanNr")
    ## Join/Merge Comet pin information to the PSM results with confidence metrcis from mokapot
    merge_results=pd.merge(input_data, mokapot_df, left_on=['SpecId','Label','ScanNr','ExpMass','CalcMass','Peptide','Proteins'],
    right_on=['SpecId','Label','ScanNr','ExpMass','CalcMass','Peptide','Proteins'],how='left',suffixes=('_in', '_out'))
    ## Filter PSMs at 1% PSM FDR level for downstream analysis
    fdr_filtered_results = merge_results[merge_results['mokapot q-value'] < 0.01]
    ## Print the number of PSMs at 1% FDR following mokapot 
    print("mokapot complete:",fdr_filtered_results.shape[0],"PSMs at 1% FDR")
    return(fdr_filtered_results)


# In[6]: Merge spectral data with the 1% FDR filtered PSMs for downstream annotation and quantification

def merge_results(fdr_filtered_results, data):
    ## Join/Merge 1% FDR filtered PSMs with MS/MS spectral information for annotation and quantification
    quant_input=pd.merge(fdr_filtered_results, data, left_on='ScanNr', right_on='ScanNum', how='left')
    return(quant_input)

# Function for Annotation of b-ions and paired y-ions. Also, includes quantification of paired y-ion features.
## Annotates the b-ion and paired y-ion fragments for monoisotopic and two isotope errors within 50 ppm (for charge z1 & z=2 fragments)
## Quantifies features (monoisotopic or isotope errors) ONLY if observed in its heavy and light forms
## Does not quantify on fragments if intensity for heavy and light features are below the noise level (median spectral intensity of all peaks in MS/MS)
## Do not quantify based on y+1 signals for they will likely have interference with other coisolated features
## Calculate median, sum (weighted average) and linear (with R^2) SILAC ratio for top most abundant fragment peaks. 
## Linear (Top3-6), Sum and Median (TopN-Top6)
## Output all annotated fragments, only quantifiable y-ion fragments, or summary file with quant results per PSM hit
## Database search based on n-terminal Protein acetylation, methionine oxidation, and Lys8 on lysines as variable modification only in search with the proteome digested with Lys-C.
## Adjustments will need to be made for other modifications.

def annotate_fragments(quant_input,Output_name):
    ## Generate lists for all annotated fragments, quantifiable fragments, and summary quant information for each PSM
    frag_list= []
    summary_quants=[]
    quantifiable_list =[]
    ## linear regression function for linear-based SILAC quantification
    linear_regressor = LinearRegression()
    ## Iterate through each MS/MS scan for annotation and quantification steps
    for i in range(0,len(quant_input)):
        ## Little timer for the progress in console
        if i % 2500 == 0:
            print(i, "out of",quant_input.shape[0],"PSMs at 1% FDR" )
        elif i == quant_input.shape[0]-1:
            print(i+1, "out of",quant_input.shape[0],"PSMs at 1% FDR" )
        ## Extract PSM information by row of dataframe
        example=quant_input.iloc[i,:]
        ## Extract scan number
        scan = example['ScanNr']
        ## Extract the precursor m/z triggered
        precursor_mz = example['PrecursorDetected']
        ## Extract the charge state information for the triggered precursor
        precursor_charge = int(example['Charge'])
        ## Array of all m/z's in the MS/MS spectra 
        mz = example['m/zArray']
        ## Array of all intensities in the MS/MS with corresponding index number in the m/s array
        intensity =example['IntensityArray']
        ## Extract the peptide sequence information
        peptide=example['Peptide']
        ## Determine the noise level in the MS/MS scan (proxy based on Bakalarski et al. J. Proteome Research 2008)
        noise = median(intensity)
        # determine the light peptide sequence 
        ##extract the peptide sequence without flanking cleavage information
        original_peptide= peptide[2:-2]
        ## replace N-terminal acetylation designations by removing the n portion
        original_peptide=original_peptide.replace("n[42.", "[42.")
        ## remove all the heavy lysine designations in the peptide sequence
        peptide_light=original_peptide.replace("[8.0142]", "")
        ## generate a light peptide list of modification indexed by amino acid location
        amino_acid_loc_dict_light={}
        if '[42.0106]' in peptide_light: 
            amino_acid_loc_dict_light['N-term'] = 42.0106
        tok_pep_light=re.findall("[A-Z](?:\[[^A-Z]+\])?", peptide_light)
        ## generate same for the reversed sequence for generating y-ion series
        tok_pep_reverse =  tok_pep_light[::-1]
        aa_index_b=re.findall("[A-Z]", peptide_light)
        aa_index_y= aa_index_b[::-1]
        for ind, aa in enumerate(tok_pep_light):
            if len(aa)>1:
                ## detemine modification mass difference
                value=float(re.search("[0-9]+\.[0-9]+",aa).group())
                ## generate indexed position for modification and its delta mass 
                amino_acid_loc_dict_light[ind] = value
        ## Same now for the reverse sequence (y-ion series)
        amino_acid_loc_dict_light_reverse={} 
        for ind, aa in enumerate(tok_pep_reverse):
            if len(aa)>1:
                value=float(re.search("[0-9]+\.[0-9]+",aa).group())
                amino_acid_loc_dict_light_reverse[ind] = value
        ### generate the heavy peptide sequence with complete labeling 
        peptide_heavy=peptide_light.replace("K", "K[8.0142]")
        ### Same thing as for the light peptide sequence
        ### Generating an indexed list of modification delta mass based on amino acid position in the forward (b-ion) and reverse (y-ion) directions
        amino_acid_loc_dict_heavy={}
        if '[42.0106]' in peptide_heavy: 
            amino_acid_loc_dict_heavy['N-term'] = 42.0106
        tok_pep_heavy=re.findall("[A-Z](?:\[[^A-Z]+\])?", peptide_heavy)
        tok_pep_heavy_reverse =  tok_pep_heavy[::-1]
        aa_index_heavy_b=re.findall("[A-Z]", peptide_heavy)
        ## heavy y-ion series same as above
        aa_index_heavy_y= aa_index_heavy_b[::-1]
        for ind, aa in enumerate(tok_pep_heavy):
            if len(aa)>1:
                value=float(re.search("[0-9]+\.[0-9]+",aa).group())
                amino_acid_loc_dict_heavy[ind] = value
        amino_acid_loc_dict_heavy_reverse={} 
        for ind, aa in enumerate(tok_pep_heavy_reverse):
            if len(aa)>1:
                value=float(re.search("[0-9]+\.[0-9]+",aa).group())
                amino_acid_loc_dict_heavy_reverse[ind] = value
        ## Based on the peptide sequence and the indexed list of delta masses which represent the modifications and their corresponding delta masses
        
        #######################      b-ion heavy and light theoretical mass annotation        ################################
        #light peptide monoisotopic mass calculator for the b-ion series
        PepLength=len(tok_pep_light)
        ### initiate b-ion series starting with the proton addition on the peptide N-terminus
        b_fragment = 1.007276
        ### lists for fragment masses, fragment annotation, and whether it is monoisotopic, C13 or 2C13 isotope errors
        masses=[]
        annotation=[]
        isotopes = []
        ### If N-terminal acetylation add the 42 Da mass to all b-ions
        if "N-term" in amino_acid_loc_dict_light:
            b_fragment =b_fragment + amino_acid_loc_dict_light["N-term"]
        ## Walk through amino acid sequence and iteratively calculate the fragment masses for monoisotopic and C13/2C13 errors. Adding on modifications when it applies.
        for i in range(0,PepLength-1):
            ## If their is a modification on the amino acid, calculate the fragment masses
            if i in amino_acid_loc_dict_light:
                ## calculate monoisotopic mass of fragment by adding amino acid mass and the modification delta mass at that amino acid location
                b_fragment =b_fragment + aa_masses_dict[aa_index_b[i]]+amino_acid_loc_dict_light[i]
                ## actual amino acid location is the i index + 1
                index_num = i+1
                ## b-ion masses for charge 1 and charge 2 fragments for monoisotopic mass and two C13 isotope errors
                masses.extend([b_fragment,b_fragment+1.0033548,b_fragment+1.0033548*2,(b_fragment+1.007276)/2,(b_fragment+1.007276+1.0033548)/2,(b_fragment+1.007276+1.0033548*2)/2])
                ## b-ion annotations 
                annotation.extend(["b"+str(index_num)+"+","b"+str(index_num)+"+","b"+str(index_num)+"+","b"+str(index_num)+"++","b"+str(index_num)+"++","b"+str(index_num)+"++"])
                ## b-ion isotope annotations
                isotopes.extend(["M0","1c13","2c13","M0","1c13","2c13" ])
            ## If their is NOT a modification on the amino acid, calculate the fragment masses
            else:
                ## calculate monoisotopic mass of fragment by adding amino acid mass (no amino acid modification to add)
                b_fragment =b_fragment + aa_masses_dict[aa_index_b[i]]
                ## actual amino acid location is the i index + 1
                index_num = i+1
                ## b-ion masses for charge 1 and charge 2 fragments for monoisotopic mass and two C13 isotope errors
                masses.extend([b_fragment,b_fragment+1.0033548,b_fragment+1.0033548*2,(b_fragment+1.007276)/2,(b_fragment+1.007276+1.0033548)/2,(b_fragment+1.007276+1.0033548*2)/2])
                ## b-ion annotations 
                annotation.extend(["b"+str(index_num)+"+","b"+str(index_num)+"+","b"+str(index_num)+"+","b"+str(index_num)+"++","b"+str(index_num)+"++","b"+str(index_num)+"++"])
                ## b-ion isotope annotations
                isotopes.extend(["M0","1c13","2c13","M0","1c13","2c13" ])
        ## Generate dataframe with all theoretical masses for b-ions (only calculate the light version) -- these do not help with quantification so not essential to annotate all heavy b-ion features
        df1 = pd.DataFrame({'TheoMZ':masses,'Annotation':annotation, 'Isotopes':isotopes,"sequence":peptide, "fragmentType" : "b-ion", "HorL" : "both"})




        #######################      light y-ion theoretical mass annotation        ################################
        #light peptide monoisotopic mass calculator for the y-ion series
        
        PepLength=len(tok_pep_reverse)
                #######################      y-ion heavy and light theoretical mass annotation        ################################
        #light peptide monoisotopic mass calculator for the y-ion series
        y_fragment = 1.007825 + 1.007825 + 15.994915 + 1.007276
        ### lists for fragment masses, fragment annotation, and whether it is monoisotopic, C13 or 2C13 isotope errors
        masses=[]
        annotation=[]
        isotopes = []
        ## Walk through amino acid sequence and iteratively calculate the fragment masses for monoisotopic and C13/2C13 errors. Adding on modifications when it applies.
        for i in range(0,PepLength-1):
            ## calculate monoisotopic mass of fragment by adding amino acid mass and the modification delta mass at that amino acid location
            if i in amino_acid_loc_dict_light_reverse:
                ## calculate monoisotopic mass of fragment by adding amino acid mass and the modification delta mass at that amino acid location
                y_fragment =y_fragment + aa_masses_dict[aa_index_y[i]]+amino_acid_loc_dict_light_reverse[i]
                ## actual amino acid location is the i index + 1
                index_num = i+1
                ## light y-ion masses for charge 1 and charge 2 fragments for monoisotopic mass and two C13 isotope errors
                masses.extend([y_fragment,y_fragment+1.0033548,y_fragment+1.0033548*2,(y_fragment+1.007276)/2,(y_fragment+1.007276+1.0033548)/2,(y_fragment+1.007276+1.0033548*2)/2])
                ## light y-ion annotations 
                annotation.extend(["y"+str(index_num)+"+","y"+str(index_num)+"+","y"+str(index_num)+"+","y"+str(index_num)+"++","y"+str(index_num)+"++","y"+str(index_num)+"++"])
                ## light y-ion isotope annotations
                isotopes.extend(["M0","1c13","2c13","M0","1c13","2c13"])
            else:
                ## calculate monoisotopic mass of fragment by adding amino acid mass (no amino acid modification to add)
                y_fragment =y_fragment + aa_masses_dict[aa_index_y[i]]
                ## actual amino acid location is the i index + 1
                index_num = i+1
                ## light y-ion masses for charge 1 and charge 2 fragments for monoisotopic mass and two C13 isotope errors
                masses.extend([y_fragment,y_fragment+1.0033548,y_fragment+1.0033548*2,(y_fragment+1.007276)/2,(y_fragment+1.007276+1.0033548)/2,(y_fragment+1.007276+1.0033548*2)/2])
                ## light y-ion annotations 
                annotation.extend(["y"+str(index_num)+"+","y"+str(index_num)+"+","y"+str(index_num)+"+","y"+str(index_num)+"++","y"+str(index_num)+"++","y"+str(index_num)+"++"])
                ## light y-ion isotope annotations
                isotopes.extend(["M0","1c13","2c13","M0","1c13","2c13"])
        ## Generate dataframe with all theoretical masses for light y-ions
        df2 = pd.DataFrame({'TheoMZ':masses,'Annotation':annotation,'Isotopes':isotopes,"sequence":peptide, "fragmentType" : "y-ion", "HorL" : "light" })
        ## Concatenate the dataframe for b-ions and light y-ions
        df3 = pd.concat([df1,df2])

        #######################      heavy y-ion theoretical mass annotation        ################################
        #heavy peptide monoisotopic mass calculator for the y-ion series
        
        PepLength=len(tok_pep_heavy_reverse)
        #heavy peptide monoisotopic mass calculator for the y-ion series
        y_fragment = 1.007825 + 1.007825 + 15.994915 + 1.007276
        ### lists for fragment masses, fragment annotation, and whether it is monoisotopic, C13 or 2C13 isotope errors
        masses=[]
        annotation=[]
        isotopes=[]
        ## Walk through amino acid sequence and iteratively calculate the fragment masses for monoisotopic and C13/2C13 errors. Adding on modifications when it applies.
        for i in range(0,PepLength-1):
            ## calculate monoisotopic mass of fragment by adding amino acid mass and the modification delta mass at that amino acid location
            if i in amino_acid_loc_dict_heavy_reverse:
                ## calculate monoisotopic mass of fragment by adding amino acid mass and the modification delta mass at that amino acid location
                y_fragment =y_fragment + aa_masses_dict[aa_index_heavy_y[i]]+amino_acid_loc_dict_heavy_reverse[i]
                ## actual amino acid location is the i index + 1
                index_num = i+1
                ## heavy y-ion masses for charge 1 and charge 2 fragments for monoisotopic mass and two C13 isotope errors
                masses.extend([y_fragment,y_fragment+1.0033548,y_fragment+1.0033548*2,(y_fragment+1.007276)/2,(y_fragment+1.007276+1.0033548)/2,(y_fragment+1.007276+1.0033548*2)/2])
                ## heavy y-ion annotations 
                annotation.extend(["y"+str(index_num)+"+","y"+str(index_num)+"+","y"+str(index_num)+"+","y"+str(index_num)+"++","y"+str(index_num)+"++","y"+str(index_num)+"++"])
                ## heavy y-ion isotope annotations
                isotopes.extend(["M0","1c13","2c13","M0","1c13","2c13"])
            else:
                ## calculate monoisotopic mass of fragment by adding amino acid mass (no amino acid modification to add)
                y_fragment =y_fragment + aa_masses_dict[aa_index_heavy_y[i]]
                ## actual amino acid location is the i index + 1
                index_num = i+1
                ## heavy y-ion masses for charge 1 and charge 2 fragments for monoisotopic mass and two C13 isotope errors
                masses.extend([y_fragment,y_fragment+1.0033548,y_fragment+1.0033548*2,(y_fragment+1.007276)/2,(y_fragment+1.007276+1.0033548)/2,(y_fragment+1.007276+1.0033548*2)/2])
                ## heavy y-ion annotations 
                annotation.extend(["y"+str(index_num)+"+","y"+str(index_num)+"+","y"+str(index_num)+"+","y"+str(index_num)+"++","y"+str(index_num)+"++","y"+str(index_num)+"++"])
                ## heavy y-ion isotope annotations
                isotopes.extend(["M0","1c13","2c13","M0","1c13","2c13"])
        ## Generate dataframe with all theoretical masses for heavy y-ions
        df4 = pd.DataFrame({'TheoMZ':masses,'Annotation':annotation,'Isotopes':isotopes,"sequence":peptide, "fragmentType" : "y-ion" , "HorL" : "heavy"})
        ## Concatenate the dataframe for b-ions and light y-ions with the heavy y-ions
        df5 = pd.concat([df3,df4])    
        ## Sort all masses based on theoretical m/z
        df6=df5.sort_values(by=['TheoMZ'])
        


        #########################           Annotation of theoretical masses to observed spectra               #########################
        ### The strategy here is to walk through the theoretical masses from the peptide and map MS/MS features and append intensities with in 50 ppm of theoretical masses
        ### Walk down both lists and track indicies such that both lists only are gone through once 
        
        ## index that maps to observed m/z and intensity arrays
        index_value = 0 
        ## track back index so only go through observed spectra from where last left off from previous annotated peak
        back_index = 0 
        val_options = []
        list_annotation_indicies = []
        list_intensities = []
        ### List of theoretical masses for peptide annotation
        theo_mass =  df6['TheoMZ'].tolist()
        ### For each theoretical fragment mass look for matching observed peak signal within MS/MS ppm tolerance
        for i in range(0,len(theo_mass)):
            ## theoretical m/z mass of fragment to match to
            value = theo_mass[i]
            ## keep looking for observed features in MS/MS if observed signal has mass less than 25 ppm above the theoretical mass and is not the end of the array of observed m/z's
            while mz[index_value] < value + value *0.000025 and index_value < len(mz) -1:
                ## if an observed MS/MS peak maps within the 50ppm tolerance of the theoretical fragment mass
                if mz[index_value] > value - value*0.000025 and mz[index_value] < value + value*0.000025:
                    ## append the options for the matched MS/MS observed peak or peaks that can be assigned to that theoretical mass
                    val_options.append(intensity[index_value])
                    ## Increase the index value 
                    index_value += 1
                ## If the observed m/z masses are below the threoretical mass lowest boundary for ppm tolerance (25 ppm below theoretical mass) then move back index up to this index value
                elif mz[index_value] < value - value*0.000025:
                    back_index = index_value
                    ## keep moving up the index value until within range
                    index_value += 1
                ## keep moving up index. this enables looking for multiple features until you are outside the 25 ppm tolerance above.
                else: 
                    index_value += 1
            ## If there are multiple observed MS/MS peaks m/z's that fall within the 50 ppm tolerance of the theoretical mass then max intensity feature is choosen
            if len(val_options) >0 :
                ## index location appended 
                list_annotation_indicies.append(i)
                ## add annotation of the theoretical mass with the max intensity of matched MS/MS peak that falls within 50 ppm of the theoretical.
                list_intensities.append(max(val_options))
                ## reset valid options for matched observed to next theoretical mass
                val_options=[]
            ## initiate new back index
            index_value = back_index
        ## reset indexes of dataframe because not all theoretical features will have an observed MS/MS
        df_final = df6.iloc[list_annotation_indicies].reset_index()
        ## append the mapped intensities for each annotation with the same indicies applied to ensure accurate mapping of correct features
        df_final['intensity'] = list_intensities
        
        ## append other relevant columns related to a give PSM/scan
        df_final["peptide_seq"] = original_peptide
        df_final["scan_num"] = scan
        df_final["precursor_mz"] = precursor_mz
        df_final["charge"] = precursor_charge
        df_final["ms2_noise"] = noise

        
        #########################           Quantification of SILAC y-ion paired features              #########################
        
        ## subset b-ion, light and heavy y-ion dataframes for merging
        light_df = df_final[df_final.HorL  == "light"]
        heavy_df = df_final[df_final.HorL  == "heavy"]
        bion_df = df_final[df_final.HorL  == "both"]
        ## merge annotated y-ion features for heavy and light for quantification (ensures that same fragment annotation with isotope designation are found in both heavy and light forms)
        quants=pd.merge(light_df, heavy_df, left_on=['Annotation','sequence','fragmentType','Isotopes', 'scan_num','precursor_mz','charge','ms2_noise'],
                            right_on=['Annotation','sequence','fragmentType','Isotopes', 'scan_num','precursor_mz','charge','ms2_noise'],
                            how='outer', suffixes =['_light','_heavy'])
        ## total MS/MS signal for all matched MS/MS features heavy and light y-ions as well as b-ions (light)
        ms2_total_intensity = sum(df_final['intensity'])
        ## this filters for finding both heavy and light y-ion features 
        quants = quants[quants.TheoMZ_light.notna()]
        quants = quants[quants.TheoMZ_heavy.notna()]
        ## extract relevant columns to increase speed
        quants1 = quants[['Annotation', 'intensity_light','intensity_heavy']]
        ##Filter for things greater than 1X noise
        quants2 = quants1[quants1['intensity_light'] > noise]
        quants3 = quants2[quants2['intensity_heavy'] > noise]
        ## calculate summaraized quantification for light and heavy features for each fragment (sum monoisotopic/C13/2C13 anntoations)
        quantifiable = quants3.groupby(by="Annotation").sum()
        ## For each fragment calculate a SILAC ratio
        quantifiable['ratio'] = quantifiable['intensity_heavy']/quantifiable['intensity_light']
        ## Calculate the y-ion fragment intensity (light  + heavy) used for determination of ordinal topN designations
        quantifiable['total'] = quantifiable['intensity_heavy'] + quantifiable['intensity_light']
        ## Assort based on fragment intensities (light + heavy) for topN designations and quantification (most intense fragments at top)
        quantifiable = quantifiable.sort_values('total',ascending=False)
        ## reset the indicies of the dataframe based on new order
        quantifiable=quantifiable.reset_index()
        ## remove y1 ions due to likely SILAC quant interference from molecular redundancy when other SILAC features are isolated in the wide window MS/MS scans
        quantifiable =quantifiable[(quantifiable['Annotation']!='y1++') & (quantifiable['Annotation']!='y1+')]
        if(quantifiable['ratio'].shape[0]>0):
            ## Quantification of PSMs for median, sum, and linear-based when 6 or more y-ion fragments annnotated for light and heavy
            if(quantifiable['ratio'].shape[0]>5):
                ## sum all heavy fragments and sum all light fragments top2-6 and topN most intense y-ion fragments and then determine SILAC ratio
                sum6_ratio=  sum(quantifiable['intensity_heavy'][0:6])/sum(quantifiable['intensity_light'][0:6])
                sumN_ratio = sum(quantifiable['intensity_heavy'])/sum(quantifiable['intensity_light'])
                ## take median of top2-6 and topN SILAC ratios of the most intense y-ion fragments respectively
                median6_ratio = median(quantifiable['ratio'][0:6])
                medianN_ratio= median(quantifiable['ratio'])
                sum5_ratio=  sum(quantifiable['intensity_heavy'][0:5])/sum(quantifiable['intensity_light'][0:5])
                median5_ratio = median(quantifiable['ratio'][0:5])
                sum4_ratio=  sum(quantifiable['intensity_heavy'][0:4])/sum(quantifiable['intensity_light'][0:4])
                median4_ratio = median(quantifiable['ratio'][0:4])
                sum3_ratio=  sum(quantifiable['intensity_heavy'][0:3])/sum(quantifiable['intensity_light'][0:3])
                median3_ratio= median(quantifiable['ratio'][0:3])
                sum2_ratio=  sum(quantifiable['intensity_heavy'][0:2])/sum(quantifiable['intensity_light'][0:2])
                median2_ratio= median(quantifiable['ratio'][0:2])
                ## fit linear regression to top3-6 most intense y-ion fragments ploting light and heavy raw intensities against one another. Computed a R^2 to access quality of fit 
                lr_fit=linear_regressor.fit(quantifiable["intensity_light"][0:6].values.reshape(-1,1),quantifiable["intensity_heavy"][0:6].values.reshape(-1,1))
                linear6_ratio = lr_fit.coef_[0][0]
                linear6_R2 = lr_fit.score(quantifiable["intensity_light"][0:6].values.reshape(-1,1),quantifiable["intensity_heavy"][0:6].values.reshape(-1,1))**2
                lr_fit5=linear_regressor.fit(quantifiable["intensity_light"][0:5].values.reshape(-1,1),quantifiable["intensity_heavy"][0:5].values.reshape(-1,1))
                linear5_ratio = lr_fit5.coef_[0][0]
                linear5_R2 = lr_fit5.score(quantifiable["intensity_light"][0:5].values.reshape(-1,1),quantifiable["intensity_heavy"][0:5].values.reshape(-1,1))**2
                lr_fit4=linear_regressor.fit(quantifiable["intensity_light"][0:4].values.reshape(-1,1),quantifiable["intensity_heavy"][0:4].values.reshape(-1,1))
                linear4_ratio = lr_fit4.coef_[0][0]
                linear4_R2 = lr_fit4.score(quantifiable["intensity_light"][0:4].values.reshape(-1,1),quantifiable["intensity_heavy"][0:4].values.reshape(-1,1))**2
                lr_fit3=linear_regressor.fit(quantifiable["intensity_light"][0:3].values.reshape(-1,1),quantifiable["intensity_heavy"][0:3].values.reshape(-1,1))
                linear3_ratio = lr_fit3.coef_[0][0]
                linear3_R2 = lr_fit3.score(quantifiable["intensity_light"][0:3].values.reshape(-1,1),quantifiable["intensity_heavy"][0:3].values.reshape(-1,1))**2
                top_s2n_H = quantifiable['intensity_heavy'].iloc[0]/noise
                top_s2n_L = quantifiable['intensity_light'].iloc[0]/noise
                avg3_s2n_H = (sum(quantifiable['intensity_heavy'][0:3]) / 3) / noise
                avg3_s2n_L = (sum(quantifiable['intensity_light'][0:3]) / 3) / noise
                avg6_s2n_H = (sum(quantifiable['intensity_heavy'][0:6]) / 6) / noise
                avg6_s2n_L = (sum(quantifiable['intensity_light'][0:6]) / 6) / noise
                totalN = sum(quantifiable['intensity_heavy'])+sum(quantifiable['intensity_light'])
                total6 = sum(quantifiable['intensity_heavy'][0:6])+sum(quantifiable['intensity_light'][0:6])
                total5 = sum(quantifiable['intensity_heavy'][0:5])+sum(quantifiable['intensity_light'][0:5])
                total4 = sum(quantifiable['intensity_heavy'][0:4])+sum(quantifiable['intensity_light'][0:4])
                total3 = sum(quantifiable['intensity_heavy'][0:3])+sum(quantifiable['intensity_light'][0:3])
                total2 = sum(quantifiable['intensity_heavy'][0:2])+sum(quantifiable['intensity_light'][0:2])
            ## Quantification of PSMs for median, sum, and linear-based for 5 y-ion fragments annnotated for light and heavy (replaces NA for all non-relevant quantifications that require more y-ion features)
            elif(quantifiable['ratio'].shape[0]==5):
                sum6_ratio=  "NaN"
                sumN_ratio = sum(quantifiable['intensity_heavy'])/sum(quantifiable['intensity_light'])
                median6_ratio = "NaN"
                medianN_ratio= median(quantifiable['ratio'])
                sum5_ratio=  sum(quantifiable['intensity_heavy'][0:5])/sum(quantifiable['intensity_light'][0:5])
                median5_ratio = median(quantifiable['ratio'][0:5])
                sum4_ratio=  sum(quantifiable['intensity_heavy'][0:4])/sum(quantifiable['intensity_light'][0:4])
                median4_ratio = median(quantifiable['ratio'][0:4])
                sum3_ratio=  sum(quantifiable['intensity_heavy'][0:3])/sum(quantifiable['intensity_light'][0:3])
                median3_ratio= median(quantifiable['ratio'][0:3])
                sum2_ratio=  sum(quantifiable['intensity_heavy'][0:2])/sum(quantifiable['intensity_light'][0:2])
                median2_ratio= median(quantifiable['ratio'][0:2])
                linear6_ratio = "NaN"
                linear6_R2 = "NaN"
                lr_fit5=linear_regressor.fit(quantifiable["intensity_light"][0:5].values.reshape(-1,1),quantifiable["intensity_heavy"][0:5].values.reshape(-1,1))
                linear5_ratio = lr_fit5.coef_[0][0]
                linear5_R2 = lr_fit5.score(quantifiable["intensity_light"][0:5].values.reshape(-1,1),quantifiable["intensity_heavy"][0:5].values.reshape(-1,1))**2
                lr_fit4=linear_regressor.fit(quantifiable["intensity_light"][0:4].values.reshape(-1,1),quantifiable["intensity_heavy"][0:4].values.reshape(-1,1))
                linear4_ratio = lr_fit4.coef_[0][0]
                linear4_R2 = lr_fit4.score(quantifiable["intensity_light"][0:4].values.reshape(-1,1),quantifiable["intensity_heavy"][0:4].values.reshape(-1,1))**2
                lr_fit3=linear_regressor.fit(quantifiable["intensity_light"][0:3].values.reshape(-1,1),quantifiable["intensity_heavy"][0:3].values.reshape(-1,1))
                linear3_ratio = lr_fit3.coef_[0][0]
                linear3_R2 = lr_fit3.score(quantifiable["intensity_light"][0:3].values.reshape(-1,1),quantifiable["intensity_heavy"][0:3].values.reshape(-1,1))**2
                top_s2n_H = quantifiable['intensity_heavy'].iloc[0]/noise
                top_s2n_L = quantifiable['intensity_light'].iloc[0]/noise
                avg3_s2n_H = (sum(quantifiable['intensity_heavy'][0:3]) / 3) / noise
                avg3_s2n_L = (sum(quantifiable['intensity_light'][0:3]) / 3) / noise
                avg6_s2n_H = "NaN"
                avg6_s2n_L = "NaN"
                totalN = sum(quantifiable['intensity_heavy'])+sum(quantifiable['intensity_light'])
                total6 = "NaN"
                total5 = sum(quantifiable['intensity_heavy'][0:5])+sum(quantifiable['intensity_light'][0:5])
                total4 = sum(quantifiable['intensity_heavy'][0:4])+sum(quantifiable['intensity_light'][0:4])
                total3 = sum(quantifiable['intensity_heavy'][0:3])+sum(quantifiable['intensity_light'][0:3])
                total2 = sum(quantifiable['intensity_heavy'][0:2])+sum(quantifiable['intensity_light'][0:2])
            ## Quantification of PSMs for median, sum, and linear-based for 4 y-ion fragments annnotated for light and heavy (replaces NA for all non-relevant quantifications that require more y-ion features)
            elif(quantifiable['ratio'].shape[0]==4):
                sum6_ratio=  "NaN"
                sumN_ratio = sum(quantifiable['intensity_heavy'])/sum(quantifiable['intensity_light'])
                median6_ratio = "NaN"
                medianN_ratio= median(quantifiable['ratio'])
                sum5_ratio=  "NaN"
                median5_ratio = "NaN"
                sum4_ratio=  sum(quantifiable['intensity_heavy'][0:4])/sum(quantifiable['intensity_light'][0:4])
                median4_ratio = median(quantifiable['ratio'][0:4])
                sum3_ratio=  sum(quantifiable['intensity_heavy'][0:3])/sum(quantifiable['intensity_light'][0:3])
                median3_ratio= median(quantifiable['ratio'][0:3])
                sum2_ratio=  sum(quantifiable['intensity_heavy'][0:2])/sum(quantifiable['intensity_light'][0:2])
                median2_ratio= median(quantifiable['ratio'][0:2])
                linear6_ratio = "NaN"
                linear6_R2 = "NaN"
                linear5_ratio = "NaN"
                linear5_R2 = "NaN"
                lr_fit4=linear_regressor.fit(quantifiable["intensity_light"][0:4].values.reshape(-1,1),quantifiable["intensity_heavy"][0:4].values.reshape(-1,1))
                linear4_ratio = lr_fit4.coef_[0][0]
                linear4_R2 = lr_fit4.score(quantifiable["intensity_light"][0:4].values.reshape(-1,1),quantifiable["intensity_heavy"][0:4].values.reshape(-1,1))**2
                lr_fit3=linear_regressor.fit(quantifiable["intensity_light"][0:3].values.reshape(-1,1),quantifiable["intensity_heavy"][0:3].values.reshape(-1,1))
                linear3_ratio = lr_fit3.coef_[0][0]
                linear3_R2 = lr_fit3.score(quantifiable["intensity_light"][0:3].values.reshape(-1,1),quantifiable["intensity_heavy"][0:3].values.reshape(-1,1))**2
                top_s2n_H = quantifiable['intensity_heavy'].iloc[0]/noise
                top_s2n_L = quantifiable['intensity_light'].iloc[0]/noise
                avg3_s2n_H = (sum(quantifiable['intensity_heavy'][0:3]) / 3) / noise
                avg3_s2n_L = (sum(quantifiable['intensity_light'][0:3]) / 3) / noise
                avg6_s2n_H = "NaN"
                avg6_s2n_L = "NaN"
                totalN = sum(quantifiable['intensity_heavy'])+sum(quantifiable['intensity_light'])
                total6 = "NaN"
                total5 = "NaN"
                total4 = sum(quantifiable['intensity_heavy'][0:4])+sum(quantifiable['intensity_light'][0:4])
                total3 = sum(quantifiable['intensity_heavy'][0:3])+sum(quantifiable['intensity_light'][0:3])
                total2 = sum(quantifiable['intensity_heavy'][0:2])+sum(quantifiable['intensity_light'][0:2])
            ## Quantification of PSMs for median, sum, and linear-based for 3 y-ion fragments annnotated for light and heavy (replaces NA for all non-relevant quantifications that require more y-ion features)
            elif(quantifiable['ratio'].shape[0]==3):
                sum6_ratio=  "NaN"
                sumN_ratio = sum(quantifiable['intensity_heavy'])/sum(quantifiable['intensity_light'])
                median6_ratio = "NaN"
                medianN_ratio= median(quantifiable['ratio'])
                sum5_ratio=  "NaN"
                median5_ratio = "NaN"
                sum4_ratio=  "NaN"
                median4_ratio = "NaN"
                sum3_ratio=  sum(quantifiable['intensity_heavy'][0:3])/sum(quantifiable['intensity_light'][0:3])
                median3_ratio= median(quantifiable['ratio'][0:3])
                sum2_ratio=  sum(quantifiable['intensity_heavy'][0:2])/sum(quantifiable['intensity_light'][0:2])
                median2_ratio= median(quantifiable['ratio'][0:2])
                linear6_ratio = "NaN"
                linear6_R2 = "NaN"
                linear5_ratio = "NaN"
                linear5_R2 = "NaN"
                linear4_ratio = "NaN"
                linear4_R2 = "NaN"
                lr_fit3=linear_regressor.fit(quantifiable["intensity_light"][0:3].values.reshape(-1,1),quantifiable["intensity_heavy"][0:3].values.reshape(-1,1))
                linear3_ratio = lr_fit3.coef_[0][0]
                linear3_R2 = lr_fit3.score(quantifiable["intensity_light"][0:3].values.reshape(-1,1),quantifiable["intensity_heavy"][0:3].values.reshape(-1,1))**2
                top_s2n_H = quantifiable['intensity_heavy'].iloc[0]/noise
                top_s2n_L = quantifiable['intensity_light'].iloc[0]/noise
                avg3_s2n_H = (sum(quantifiable['intensity_heavy'][0:3]) / 3) / noise
                avg3_s2n_L = (sum(quantifiable['intensity_light'][0:3]) / 3) / noise
                avg6_s2n_H = "NaN"
                avg6_s2n_L = "NaN"
                totalN = sum(quantifiable['intensity_heavy'])+sum(quantifiable['intensity_light'])
                total6 = "NaN"
                total5 = "NaN"
                total4 = "NaN"
                total3 = sum(quantifiable['intensity_heavy'][0:3])+sum(quantifiable['intensity_light'][0:3])
                total2 = sum(quantifiable['intensity_heavy'][0:2])+sum(quantifiable['intensity_light'][0:2])
            ## Quantification of PSMs for median, sum, and linear-based for 2 y-ion fragments annnotated for light and heavy (replaces NA for all non-relevant quantifications that require more y-ion features)
            elif(quantifiable['ratio'].shape[0]==2):
                sum6_ratio=  "NaN"
                sumN_ratio = sum(quantifiable['intensity_heavy'])/sum(quantifiable['intensity_light'])
                median6_ratio = "NaN"
                medianN_ratio= median(quantifiable['ratio'])
                sum5_ratio=  "NaN"
                median5_ratio = "NaN"
                sum4_ratio=  "NaN"
                median4_ratio = "NaN"
                sum3_ratio=  "NaN"
                median3_ratio= "NaN"
                sum2_ratio=  sum(quantifiable['intensity_heavy'][0:2])/sum(quantifiable['intensity_light'][0:2])
                median2_ratio= median(quantifiable['ratio'][0:2])
                linear6_ratio = "NaN"
                linear6_R2 = "NaN"
                linear5_ratio = "NaN"
                linear5_R2 = "NaN"
                linear4_ratio = "NaN"
                linear4_R2 = "NaN"
                linear3_ratio = "NaN"
                linear3_R2 = "NaN"
                top_s2n_H = quantifiable['intensity_heavy'].iloc[0]/noise
                top_s2n_L = quantifiable['intensity_light'].iloc[0]/noise
                avg3_s2n_H = "NaN"
                avg3_s2n_L = "NaN"
                avg6_s2n_H = "NaN"
                avg6_s2n_L = "NaN"   
                totalN = sum(quantifiable['intensity_heavy'])+sum(quantifiable['intensity_light'])
                total6 = "NaN"
                total5 = "NaN"
                total4 = "NaN"
                total3 = "NaN"
                total2 = sum(quantifiable['intensity_heavy'][0:2])+sum(quantifiable['intensity_light'][0:2])                
            ## Quantification of PSMs for median, sum, and linear-based for 5 y-ion fragments annnotated for light and heavy (replaces NA for all non-relevant quantifications that require more y-ion features)
            elif(quantifiable['ratio'].shape[0]==1):
                sum6_ratio=  "NaN"
                sumN_ratio = sum(quantifiable['intensity_heavy'])/sum(quantifiable['intensity_light'])
                median6_ratio = "NaN"
                medianN_ratio= median(quantifiable['ratio'])
                sum5_ratio=  "NaN"
                median5_ratio = "NaN"
                sum4_ratio=  "NaN"
                median4_ratio = "NaN"
                sum3_ratio=  "NaN"
                median3_ratio= "NaN"
                sum2_ratio=  "NaN"
                median2_ratio= "NaN"
                linear6_ratio = "NaN"
                linear6_R2 = "NaN"
                linear5_ratio = "NaN"
                linear5_R2 = "NaN"
                linear4_ratio = "NaN"
                linear4_R2 = "NaN"
                linear3_ratio = "NaN"
                linear3_R2 = "NaN"
                top_s2n_H = quantifiable['intensity_heavy'].iloc[0]/noise
                top_s2n_L = quantifiable['intensity_light'].iloc[0]/noise
                avg3_s2n_H = "NaN"
                avg3_s2n_L = "NaN"
                avg6_s2n_H = "NaN"
                avg6_s2n_L = "NaN"
                totalN = sum(quantifiable['intensity_heavy'])+sum(quantifiable['intensity_light'])
                total6 = "NaN"
                total5 = "NaN"
                total4 = "NaN"
                total3 = "NaN"
                total2 = "NaN"
        ### if not quantifiable y-ion fragments are observed fill in all computed quants with NA values 
        else:
            sum6_ratio=  "NaN"
            sumN_ratio = "NaN"
            median6_ratio = "NaN"
            medianN_ratio= "NaN"
            sum5_ratio=  "NaN"
            median5_ratio = "NaN"
            sum4_ratio=  "NaN"
            median4_ratio = "NaN"
            sum3_ratio=  "NaN"
            median3_ratio= "NaN"
            sum2_ratio=  "NaN"
            median2_ratio= "NaN"
            linear6_ratio = "NaN"
            linear6_R2 = "NaN"
            linear5_ratio = "NaN"
            linear5_R2 = "NaN"
            linear4_ratio = "NaN"
            linear4_R2 = "NaN"
            linear3_ratio = "NaN"
            linear3_R2 = "NaN"
            top_s2n_H = "NaN"
            top_s2n_L = "NaN"
            avg3_s2n_H = "NaN"
            avg3_s2n_L = "NaN"
            avg6_s2n_H = "NaN"
            avg6_s2n_L = "NaN"  
            totalN = "NaN"
            total6 = "NaN"
            total5 = "NaN"
            total4 = "NaN"
            total3 = "NaN"
            total2 = "NaN"
        ### add all quantification values to dataframe
        quantifiable["sumN_ratio"] = sumN_ratio
        quantifiable["sum6_ratio"] = sum6_ratio
        quantifiable["sum5_ratio"] = sum5_ratio
        quantifiable["sum4_ratio"] = sum4_ratio
        quantifiable["sum3_ratio"] = sum3_ratio
        quantifiable["sum2_ratio"] = sum2_ratio
        quantifiable["medianN_ratio"] = medianN_ratio
        quantifiable["median6_ratio"] = median6_ratio
        quantifiable["median5_ratio"] = median5_ratio
        quantifiable["median4_ratio"] = median4_ratio
        quantifiable["median3_ratio"] = median3_ratio
        quantifiable["median2_ratio"] = median2_ratio
        quantifiable["linear6_ratio"] = linear6_ratio
        quantifiable["linear6_R2"] = linear6_R2
        quantifiable["linear5_ratio"] = linear5_ratio
        quantifiable["linear5_R2"] = linear5_R2
        quantifiable["linear4_ratio"] = linear4_ratio
        quantifiable["linear4_R2"] = linear4_R2
        quantifiable["linear3_ratio"] = linear3_ratio
        quantifiable["linear3_R2"] = linear3_R2
        quantifiable["num_quantFrag"] = quantifiable.shape[0]
        quantifiable["top_s2n_H"] = top_s2n_H
        quantifiable["top_s2n_L"] = top_s2n_L
        quantifiable["avg3_s2n_H"] = avg3_s2n_H
        quantifiable["avg3_s2n_L"] = avg3_s2n_L
        quantifiable["avg6_s2n_H"] = avg6_s2n_H
        quantifiable["avg6_s2n_L"] = avg6_s2n_L
        quantifiable["totalN"] = totalN
        quantifiable["total6"] = total6
        quantifiable["total5"] = total5
        quantifiable["total4"] = total4
        quantifiable["total3"] = total3
        quantifiable["total2"] = total2

        ## add relevant peptide and scan information to dataframe for export
        quantifiable["peptide_seq"] = original_peptide
        quantifiable["scan_num"] = scan
        quantifiable["precursor_mz"] = precursor_mz
        quantifiable["charge"] = precursor_charge
        quantifiable["ms2_noise"] = noise
        quantifiable["ms2_total"] = ms2_total_intensity
        ## dataframe for all the PSM information for joining downstream to quant information
        ex= pd.DataFrame(data = [example.values] , columns = example.index)
        ## remove all observed arrayed data for exporting. leaving them in will ot enable export
        del ex['m/zArray']
        del ex['IntensityArray']    
        ## rename all the columns
        summary_start=quantifiable[["peptide_seq", "scan_num","precursor_mz","charge","ms2_total","ms2_noise","sumN_ratio","sum6_ratio","sum5_ratio","sum4_ratio","sum3_ratio","sum2_ratio","medianN_ratio","median6_ratio","median5_ratio","median4_ratio","median3_ratio","median2_ratio","linear6_ratio","linear6_R2","linear5_ratio","linear5_R2","linear4_ratio","linear4_R2","linear3_ratio","linear3_R2","num_quantFrag","top_s2n_H","top_s2n_L","avg3_s2n_H","avg3_s2n_L","avg6_s2n_H","avg6_s2n_L", "totalN", "total6", "total5", "total4", "total3", "total2"]].drop_duplicates()
        ##  merging all the identification information to the quantification information calculated here
        summary= pd.merge(ex, summary_start, left_on="ScanNr",right_on="scan_num",how="outer")
        ## append all annotated fragments, summary quants, and quantifiable quantifications
        frag_list.append(df_final)
        summary_quants.append(summary)
        quantifiable_list.append(quantifiable)       
    ## Concatinate all matched information for this PSM to the rest of the data
    frag_df=pd.concat(frag_list)
    quantifiable_df = pd.concat(quantifiable_list)
    summary_df=pd.concat(summary_quants)
    return(frag_df,summary_df,quantifiable_df)

################################        Mapping the MS1 Dinosaur identified features to the identified PSMs for light and heavy SILAC pairs        ################################

### Here, we read all identified features from a standard output from Dinosaur and map them to the PSM identifications from above pipeline. 
### Calculate the theoretical masses of the monoisotopic mass for the heavy and light features that are mapped to Dinosaurs monoisotopic mass calculated.
### The observed PSM retention time must fall within the bounds of the Dinosaur feature and be within 50 ppm  

def ms1_quant(quant_input, dinosaur_file):
    ## read in the .csv output from Dinosaur (Dinosaur run just on the mzML itself)
    dinosaur = pd.read_csv(dinosaur_file, sep = '\t')
    summary_quants=[]
    ### Iterate through all PSMs to append the MS1 quantifications
    for i in range(0,len(quant_input)):
        ## Track progress through all the PSMs
        if i % 5000 == 0:
            print(i, "out of",quant_input.shape[0],"MS1 quant from Dinosaur added" )
        elif i == quant_input.shape[0]-1:
            print(i+1, "out of",quant_input.shape[0],"MS1 quant from Dinosaur added" )
        ## Extract by row from PSM dataframe
        test_example = quant_input.iloc[i]
        scan_num = test_example["ScanNr"]
        ## Calculate the theoretical mass of the monoisotopic peak for matching to the Dinosaur data
        theo_mz = test_example["CalcMass"] / test_example["Charge"] + (1.007825 * (test_example["Charge"] -1))/test_example["Charge"]
        if len(re.findall("\\[8.0142\\]", test_example["Peptide"])) > 0:
            psm=dinosaur[(dinosaur["rtStart"]<test_example["RetentionTime"])&(dinosaur["charge"]==test_example["Charge"])  & (dinosaur["rtEnd"]>test_example["RetentionTime"]) & (dinosaur["mz"]> theo_mz-dinosaur["mz"]*0.000025) & (dinosaur["mz"]< theo_mz+dinosaur["mz"]*0.000025) ]
            if psm.shape[0]>1:
                index_num4=psm["intensityApex"].idxmax()
                psm=psm[psm.index == index_num4]
            original_peptide=  test_example["Peptide"][2:-2]
            NumK=len(re.findall("K", original_peptide))
            light_mass = theo_mz - 8.0142 /test_example["Charge"]*NumK
            match=dinosaur[(dinosaur["rtStart"]<test_example["RetentionTime"])&(dinosaur["charge"]==test_example["Charge"])  & (dinosaur["rtEnd"]>test_example["RetentionTime"]) & (dinosaur["mz"]>light_mass-dinosaur["mz"]*0.000025) & (dinosaur["mz"]< light_mass+dinosaur["mz"]*0.000025) ]
            if match.shape[0]>1:
                index_num2=match["intensityApex"].idxmax()
                match=match[match.index == index_num2]
            match = match.assign(scan=scan_num)
            psm = psm.assign(scan=scan_num)
            val_merge=pd.merge(match, psm, left_on='scan', right_on='scan', how='outer', suffixes=("_light", "_heavy"))
        elif len(re.findall("\\[8.0142\\]", test_example["Peptide"])) == 0:
            psm=dinosaur[(dinosaur["rtStart"]<test_example["RetentionTime"])&(dinosaur["charge"]==test_example["Charge"])  & (dinosaur["rtEnd"]>test_example["RetentionTime"]) & (dinosaur["mz"]> theo_mz-dinosaur["mz"]*0.000025) & (dinosaur["mz"]< theo_mz+dinosaur["mz"]*0.000025) ]
            if psm.shape[0]>1:
                index_num3=psm["intensityApex"].idxmax()
                psm=psm[psm.index == index_num3]
            original_peptide=  test_example["Peptide"][2:-2]
            NumK=len(re.findall("K", original_peptide))
            heavy_mass = theo_mz + 8.0142 /test_example["Charge"]*NumK
            match=dinosaur[(dinosaur["rtStart"]<test_example["RetentionTime"])&(dinosaur["charge"]==test_example["Charge"])  & (dinosaur["rtEnd"]>test_example["RetentionTime"]) & (dinosaur["mz"]>heavy_mass-dinosaur["mz"]*0.000025) & (dinosaur["mz"]< heavy_mass+dinosaur["mz"]*0.000025) ]
            if match.shape[0]>1:
                index_num4=match["intensityApex"].idxmax()
                match=match[match.index == index_num4]
            match = match.assign(scan=scan_num)
            psm = psm.assign(scan=scan_num)
            val_merge=pd.merge(psm, match, left_on='scan', right_on='scan', how='outer', suffixes=("_light", "_heavy"))
        summary_quants.append(val_merge)    
    dino_df_quants=pd.concat(summary_quants)
    return(dino_df_quants)


def merge_ms1(dino_df_quants, summary_df):
    final_summary_df=pd.merge(summary_df, dino_df_quants, left_on='scan_num', right_on='scan', how='left')
    return(final_summary_df)


###Main function

def main(mzML_file,Pin_file,dinosaur_file,Output_name):
    start_time = datetime.now()
    spectra = get_mzml(mzML_file)
    print("mzml import")
    print("Comet pin import")
    fdr_filtered_results = get_pin_mokapot_filter(Pin_file)
    merge_df = merge_results(fdr_filtered_results,spectra)
    print("merge success")
    annotate_df,summary_df, quantifiable_pep_df = annotate_fragments(merge_df, Output_name)
    print("annotate success")
    allFrags_name= Output_name.replace('.csv', '_all_frags.csv')
    quantifiable_name = Output_name.replace('.csv', '_quant_frags.csv')
    annotate_df.to_csv(allFrags_name, sep=',') 
    quantifiable_pep_df.to_csv(quantifiable_name, sep=',')
    print("fragment annotation exports success")
    dino_df_quants = ms1_quant(merge_df, dinosaur_file)
    print("ms1 Dinosaur feature map success")
    final_summary_df = merge_ms1(dino_df_quants,summary_df)
    print("merge ms1 to summary quants")
    final_summary_df.to_csv(Output_name, sep=',') 
    print("export success")  
    end_time = datetime.now()
    print('Duration: {}'.format(end_time - start_time))
    
    
if __name__ == "__main__":
    ###Run command
    mzML_file = sys.argv[1]
    Pin_file = sys.argv[2]
    dinosaur_file = sys.argv[3]
    Output_name = sys.argv[4]
    main(mzML_file,Pin_file,dinosaur_file,Output_name)

#python Coiso_K0K8.py "filename.mzML" "filename.pin" "filename.features.tsv" "filename_MS2_Quant.csv"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "isotopes.h"

/*
   A program for calculating isotope distributions
   2013 James E. Redman
   www.kombyonyx.com
   
   Based on J.A.Yergey, Int. J. Mass Spectrom. Ion Phys. 1983, 52, 337-349
*/


//these are two constants used for calculating Gaussian peaks
const double GC1 = 0.9394372785;
const double GC2 = 2.772588722;


using namespace std;

double fdiff(int f1, int f2)    //evaluate f1!/f2!
{
   unsigned int result = 1;     //use unsigned to get more range
   if (f1 >= f2)
   {
      for (int i = f1; i > f2; i--)
      {
         result *= i;
      }
      return result;
   }
   else
   {
      for (int i = f2; i > f1; i--)
      {
         result *= i;
      }
      return (double) 1.0 / (double) result;
   }
}

inline double powdiff(double r, double f1, double f2)   // return r^(f1-f2)
{
   return pow(r, f1 - f2);
}

int comp_isotope_abundance(const void *p1, const void *p2)      //function for comparing isotope abundances (for sorting)
{
   double ans = ((isotope *) p2)->abundance - ((isotope *) p1)->abundance;
   if (ans > 0)
      return 1;
   else if (ans < 0)
      return -1;
   return 0;
}

isotope::isotope(double _mass, double _abundance):mass(_mass), abundance(_abundance)
{
}

BaseSpectrum::~BaseSpectrum()
{
}

PeakList::PeakList()
{
}

PeakList::PeakList(int _npeaks)
{
   peaks.resize(_npeaks);
}

int PeakList::size()
{
   return peaks.size();
}

void PeakList::resize(int _size)
{
   peaks.resize(_size);
}

double PeakList::intensity(double mass)
{
   peaks.push_back(peak(mass, 0));      //temporary fudge  - fill in this fn later
   return peaks.back().abundance;       // should return reference to the intensity at a particular mass
}

peak & PeakList::operator[](int idx)    //return peak [idx]
{
   return peaks[idx];
}

void PeakList::push_back(peak & ob)     //add a peak at the end
{
   peaks.push_back(ob);
}

void PeakList::push_front(peak & ob)
{
   peaks.push_front(ob);
}

void PeakList::pop_back()
{
   peaks.pop_back();
}

void PeakList::pop_front()
{
   peaks.pop_front();
}

IntensityList::IntensityList()
{
}

IntensityList::IntensityList(int _npeaks)
{
   peaks.resize(_npeaks);
}

int IntensityList::size()
{
   return peaks.size();
}

void IntensityList::resize(int _npeaks)
{
   peaks.resize(_npeaks);
}

double &IntensityList::operator[] (int idx)
{
   return peaks[idx];
}

double IntensityList::intensity(double mass)
{
   return 0;                    //a dummy return value for now - need to match peaks and return appropriate peak intensity
}

void IntensityList::push_back(peak & ob)        //add a peak at the end -- not really work correctly as mass is ignored
{
   peaks.push_back(ob.abundance);
}

void IntensityList::push_front(peak & ob)       //add peak at the beginning - ignoring the mass
{
   peaks.push_front(ob.abundance);
}

void IntensityList::pop_back()
{
   peaks.pop_back();
}

void IntensityList::pop_front()
{
   peaks.pop_front();
}

GaussianList::GaussianList():fwhm(0.1)
{
}

GaussianList::GaussianList(int _npeaks):fwhm(0.5), PeakList(_npeaks)
{
}

double GaussianList::intensity(double mass)     //calculate intensity using a sum of gaussians of width fwhm
{
   deque < peak >::iterator lp = peaks.begin();
   double _intensity = 0;
   while (lp != peaks.end())
   {
      _intensity +=
         lp->abundance * GC1 * exp(-pow((mass - lp->mass) / fwhm, 2) * GC2) /
         fwhm;
      lp++;
   }
   return _intensity;
}

AtomIsoAbun::AtomIsoAbun():niso(NULL), z(0), isotopes(NULL)
{
   symbol[0] = '\0';
}

AtomIsoAbun::AtomIsoAbun(int _z, int _niso):z(_z), niso(_niso)
{
   isotopes = new isotope[niso];        //create arrays to hold masses and abundances
}

AtomIsoAbun::~AtomIsoAbun()
{
   delete[]isotopes;
}

AtomIsoAbun::AtomIsoAbun(const AtomIsoAbun & ob)
{
   niso = ob.niso;
   z = ob.z;
   isotopes = new isotope[niso];
   for (int i = 0; i < niso; i++)
   {
      isotopes[i] = ob.isotopes[i];
   }
   strcpy(symbol, ob.symbol);
}

AtomIsoAbun & AtomIsoAbun::operator=(AtomIsoAbun & ob)
{
   if (this != &ob)
   {
      delete[]isotopes;
      niso = ob.niso;
      z = ob.z;
      isotopes = new isotope[niso];
      for (int i = 0; i < niso; i++)
      {
         isotopes[i] = ob.isotopes[i];
      }
      strcpy(symbol, ob.symbol);
   }
   return *this;
}

bool AtomIsoAbun::operator<(const AtomIsoAbun & ob) const       // allow atoms to be compared/sorted by element symbol
{
   return strcmp(symbol, ob.symbol) < 0 ? true : false;
}

bool AtomIsoAbun::operator==(const AtomIsoAbun & ob) const              // == operator for atoms, does the comparison according to the element symbol
{
   return !strcmp(symbol, ob.symbol);
}

void AtomIsoAbun::Setniso(int _niso)
{
   delete[]isotopes;
   niso = _niso;
   isotopes = new isotope[niso];
}

int AtomIsoAbun::Getniso()
{
   return niso;
}

void AtomIsoAbun::Sort_abundance()
{
   qsort(isotopes, niso, sizeof(isotope), comp_isotope_abundance);
}

ostream & operator<<(ostream & stream, AtomIsoAbun & ob)
{
   stream << ob.z << ' ' << ob.symbol << '\n';
   stream << ob.niso << '\n';
   for (int i = 0; i < ob.niso; i++)
   {
      stream << ob.isotopes[i].mass << ' ' << ob.isotopes[i].
         abundance << '\n';
   }
   return stream;
}

istream & operator>>(istream & stream, AtomIsoAbun & ob)
{
   delete[]ob.isotopes;
   stream >> ob.z >> ob.symbol;
   stream >> ob.niso;
   ob.isotopes = new isotope[ob.niso];
   for (int i = 0; i < ob.niso; i++)
   {
      stream >> ob.isotopes[i].mass >> ob.isotopes[i].abundance;
   }
   return stream;
}

MolComposition::MolComposition():totalnat(0), nat(NULL), atom(NULL), z(0)
{
}

MolComposition::MolComposition(int _totalnat)
{
   totalnat = _totalnat;
   atom = new AtomIsoAbun[totalnat];
   nat = new int[totalnat];
   z = 0;
}

MolComposition::MolComposition(const MolComposition & ob)
{
   totalnat = ob.totalnat;
   z = ob.z;
   atom = new AtomIsoAbun[totalnat];
   nat = new int[totalnat];
   for (int i = 0; i < totalnat; i++)
   {
      nat[i] = ob.nat[i];
      atom[i] = ob.atom[i];
   }
}

MolComposition::~MolComposition()
{
   delete[]nat;
   delete[]atom;
}

MolComposition & MolComposition::operator=(MolComposition & ob)
{
   if (this != &ob)
   {
      delete[]nat;
      delete[]atom;
      totalnat = ob.totalnat;
      z = ob.z;
      atom = new AtomIsoAbun[totalnat];
      nat = new int[totalnat];
      for (int i = 0; i < totalnat; i++)
      {
         nat[i] = ob.nat[i];
         atom[i] = ob.atom[i];
      }
   }
   return *this;
}

ostream & operator<<(ostream & stream, MolComposition & ob)
{
   stream << ob.totalnat << '\n';
   stream << ob.z << '\n';
   for (int i = 0; i < ob.totalnat; i++)
   {
      stream << ob.nat[i] << '\n';
      stream << ob.atom[i];
   }
   return stream;
}

istream & operator>>(istream & stream, MolComposition & ob)
{
   delete[]ob.nat;
   delete[]ob.atom;
   stream >> ob.totalnat;
   stream >> ob.z;
   ob.atom = new AtomIsoAbun[ob.totalnat];
   ob.nat = new int[ob.totalnat];
   for (int i = 0; i < ob.totalnat; i++)
   {
      stream >> ob.nat[i];
      stream >> ob.atom[i];
   }
   return stream;
}

void MolComposition::Settotalnat(int _totalnat)
{
   delete[]nat;
   delete[]atom;
   totalnat = _totalnat;
   atom = new AtomIsoAbun[totalnat];
   nat = new int[totalnat];
}

int MolEnsemble::GenerateEnsemble(MolComposition & comp, double thr)    //calculate the isotopomer distribution, with probability threshold thr
{                               //calculates masses on the fly too
   totalnat = comp.totalnat;
   delete[]elements;            //get rid of any existing array of atom definitions and list of isotopomers
   molecule.clear();
   elements = new AtomEnsemble[totalnat];       //create a new ensemble for each element
   for (int i = 0; i < totalnat; i++)
   {                            //loop thru each atom
      AtomDistribution temp;    //create a new working distribution of atoms...
      double maxabundance;      //maximum abundance found so far
      double *localmax = new double[comp.atom[i].Getniso()];    //an array containing the local abundance maxima
      int maxidx = 0;
      temp.number.resize(comp.atom[i].Getniso(), 0);    // ...all with zero contents
      elements[i].atomdef = comp.atom[i];       //transfer the atom isotope definitions
      vector < int >::iterator j = temp.number.begin(); //iterator to the isotope under consideration
      const vector < int >::iterator end = temp.number.end() - 1;       //an iterator pointing at the last element of the array
      *j = comp.nat[i];         //initialize first (most abundant) isotope with the total number of atoms of this element
      maxabundance = temp.abundance = pow(comp.atom[i].isotopes[0].abundance, *j);      //calculate the abundance of the first permutation
      temp.CalculateMass(&comp.atom[i]);        //calculate mass of first permutation
      elements[i].distribution.push_back(temp); //add the initial state to the list
      localmax[0] = temp.abundance;
      bool backtrack = false;
      do
      {
         if (j != end && !backtrack)
         {                      //if not on the final isotope
            (*j)--;             //decrement number of atoms
            j++;                //move to the next isotope
            maxidx++;
            (*j)++;             //increment the number of atoms of the new isotope
            list < AtomDistribution >::iterator last =
               elements[i].distribution.end();
            last--;             // make an iterator to the last distribution added
            temp.abundance = last->abundance;
            for (int k = 0; k < comp.atom[i].Getniso(); k++)
            {                   //calculate abundance - based on the last calculated distribution
               temp.abundance *= fdiff(last->number[k], temp.number[k]);
               temp.abundance *=
                  powdiff(comp.atom[i].isotopes[k].abundance, temp.number[k],
                          last->number[k]);
            }
            temp.CalculateMass(&comp.atom[i]);
            if (temp.abundance > maxabundance * thr)
            {                   //add to list if it is abundant enough
               elements[i].distribution.push_back(temp);        //add the state to the list
               if (temp.abundance > maxabundance)
               {
                  maxabundance = temp.abundance;
               }
            }
            localmax[maxidx] = temp.abundance;  //have just gone to next isotope - therefore always a new maximum (as it would have been zero)
            if (temp.abundance > 1)
            {                   //check that value is plausible and die if it isn't
               delete[]elements;
               elements = NULL;
               delete[]localmax;
               return 6;        //cleanup and return error if something (math error) has gone wrong
            }
         }
         else
         {
            backtrack = false;
            vector < int >::iterator oldj = j;
            bool thru;
            do
            {
               if (maxidx > 0 && localmax[maxidx] > localmax[maxidx - 1])
               {                // check whether to carry back the maximum abundance to the previous level
                  localmax[maxidx - 1] = localmax[maxidx];
                  thru = false;
               }
               else
               {
                  thru = true;
               }
               j--;
               maxidx--;
            }
            while (j >= temp.number.begin() && !*j);    //go back to find the previous isotope with something in it, but stop at the beginning

            if (j >= temp.number.begin())
            {                   //check that have not run to the start of the array
               int carry = *oldj;       //this swaps the values of the last slot and the new adjacent slot (use a swap in case the two slots are the same)
               *oldj = *(j + 1);        //empty the last slot
               *(j + 1) = carry;        //transfer all the stuff from the end (or current isotope) back to the adjacent empty slot
            }
            if (thru && localmax[maxidx + 1] < maxabundance * thr)
            {                   //if it is pointless decreasing the number of the current isotope...
               *j += *(j + 1);  // sum up adjacent levels
               *(j + 1) = 0;
               backtrack = true;        //go up a level
            }
         }
      }
      while (j >= temp.number.begin()); //carry on until there is nothing left in the first isotope
      delete[]localmax;
   }
   //now multiplex the atom ensembles
   list < AtomDistribution >::iterator * atselect = new list < AtomDistribution >::iterator[totalnat];  //an array of list iterators to point at the selected atom distributions
   double *dummyabun = new double[totalnat + 1], *tempabun;
   tempabun = dummyabun + 1;    //create an abundance array with an element at -1 for initialization
   tempabun[-1] = 1;
   double *dummymass = new double[totalnat + 1], *tempmass;
   tempmass = dummymass + 1;    //mass array with -1 element for initialization
   tempmass[-1] = 0;
   int idx = 0;
   double maxabundance = 0;
   MolDistribution currmol;     //an object in which to construct the molecule
   currmol.atomlist.resize(totalnat);
   currmol.z = comp.z;          //set the charge to be the same for all molecules
   atselect[0] = elements[0].distribution.begin();      // initialize to first distribution of first element
   bool backtrack = false;
   do
   {
      if (!backtrack)
      {
         if (idx < totalnat)
         {                      //it's an incomplete molecule...
            tempabun[idx] = tempabun[idx - 1] * atselect[idx]->abundance;       //calculate incomplete molecule's mass and abundance
            tempmass[idx] = tempmass[idx - 1] + atselect[idx]->mass;
            if (!(backtrack = tempabun[idx] < maxabundance * thr))
            {                   //set backtrack flag if the incomplete molecule is not abundant enough
               idx++;           //if not backtracking then move forward
               if (idx < totalnat)
                  atselect[idx] = elements[idx].distribution.begin();
            }
         }
         else
         {                      //it's a complete molecule - must be abundant enough otherwise would have been chucked out on previous cycle
            for (int i = 0; i < totalnat; i++)
            {                   //create the composition of the molecule from combination of the individual atoms ensembles
               currmol.atomlist[i] = &*atselect[i];
            }
            currmol.abundance = tempabun[idx - 1];
            currmol.mass = tempmass[idx - 1] - comp.z * ElectronMass;   //correct mass for loss or gain of electron
            molecule.push_back(currmol);        //add to list
            if (currmol.abundance > maxabundance)
               maxabundance = currmol.abundance;
            idx--;              //move back to a valid value of idx
            backtrack = true;
         }
      }
      else
      {
         atselect[idx]++;       //try the next atom distribution
         if (atselect[idx] != elements[idx].distribution.end())
         {
            backtrack = false;  //if the distribution is valid, go forward...
         }
         else
         {
            idx--;              //if not, then go to the previous element, and backtrack again
         }
      }
   }
   while (idx >= 0);
   delete[]dummyabun;
   delete[]dummymass;
   delete[]atselect;
   return 0;
}

void MolEnsemble::MakeSpectrum(BaseSpectrum * obPtr, double degen)      //generate a mass spectrum in *obPtr, merging peaks within degen units
{
   molecule.sort();
   double lowmtoz, totalintensity, avemtoz;
   list < MolDistribution >::iterator i = molecule.begin();     //merge peaks within degen, with the weight average mass, and total intensity

   if (spectype == masstocharge)
   {
      while (i != molecule.end() && !i->z)
      {
         i++;
      }                         //if calculating mass-to-charge skip over molecules with no charge
   }
   if (i == molecule.end())
   {
      return;
   }                            //breakout if the end of the list has been hit

   int ez = (spectype == mass) ? 1 : i->z;      //if the spectrum type is mass then force the charge used in the calculations to +1, regardless of the actual charge
   lowmtoz = i->mass / ez;      // do I need to be careful? will this mess up if the ensemble contains molecules with different charge
   totalintensity = avemtoz = 0;

   while (i != molecule.end())
   {
      if (spectype == mass || i->z)
      {                         //if spectrum is mass-to-charge then must skip over any uncharged molecules
         ez = (spectype == mass) ? 1 : i->z;
         if (fabs(i->mass / ez - lowmtoz) <= degen)
         {                      //check whether adjacent peaks differ by less than degen, and merge them if they are
            totalintensity += i->abundance;
            avemtoz += i->abundance * i->mass / ez;     //weight according to abundance
         }
         else
         {
            peak ptemp(avemtoz / totalintensity, totalintensity);
            obPtr->push_back(ptemp);
            totalintensity = i->abundance;
            avemtoz = i->abundance * i->mass / ez;
         }
         lowmtoz = i->mass / ez;
      }
      i++;
   }
   if (totalintensity)
   {
      peak ptemp(avemtoz / totalintensity, totalintensity);
      obPtr->push_back(ptemp);  //add the final peak, provided that it exists
   }
}

double AtomDistribution::CalculateMass(AtomIsoAbun * ob)
{
   mass = 0;
   for (unsigned int i = 0; i < number.size(); i++)
   {
      mass += ob->isotopes[i].mass * number[i];
   }
   return mass;
}

ostream & operator<<(ostream & stream, AtomDistribution & ob)
{
   for (vector < int >::iterator i = ob.number.begin(); i < ob.number.end();
        i++)
   {
      stream << ' ' << *i;
   }
   return stream;
}

AtomEnsemble::AtomEnsemble()
{
}

AtomEnsemble::AtomEnsemble(int _z, int _niso)
{
   atomdef.Setniso(_niso);
   atomdef.z = _z;
}

AtomEnsemble::AtomEnsemble(AtomIsoAbun & _atomdef):atomdef(_atomdef)
{
}

ostream & operator<<(ostream & stream, AtomEnsemble & ob)
{
   stream << ob.atomdef;
   for (list < AtomDistribution >::iterator i = ob.distribution.begin();
        i != ob.distribution.end(); i++)
   {
      stream << *i;
   }
   return stream;
}

double MolDistribution::CalculateMass() //calculate molecule mass from sum of masses of pre-calculated atom distributions
{
   mass = 0;
   for (vector < AtomDistribution * >::iterator i = atomlist.begin();
        i != atomlist.end(); i++)
   {
      mass += (*i)->mass;
   }
   mass -= z * ElectronMass;    //take into account any added or removed electrons
   return mass;
}

bool MolDistribution::operator<(const MolDistribution & ob) const       //compare by mass (NOT mass to charge)
{                               //this may cause things to break if molecules have different charges
   return mass < ob.mass;
}

ostream & operator<<(ostream & stream, MolDistribution & ob)
{
   for (vector < AtomDistribution * >::iterator i = ob.atomlist.begin();
        i < ob.atomlist.end(); i++)
   {
      stream << **i;
   }
   stream << ' ' << ob.z;
   int p = stream.precision(8); //boost to 8 digits of precision for the mass
   stream << ' ' << ob.mass;
   stream.precision(p);
   stream << ' ' << ob.abundance << '\n';
   return stream;
}

MolEnsemble::MolEnsemble():elements(NULL), totalnat(0), spectype(mass)
{
}

MolEnsemble::MolEnsemble(MolComposition & comp, double thr)
{
   GenerateEnsemble(comp, thr);
}

MolEnsemble::MolEnsemble(const MolEnsemble & ob)
{
   totalnat = ob.totalnat;
   spectype = ob.spectype;
   elements = new AtomEnsemble[totalnat];
   for (int i = 0; i < totalnat; i++)
      elements[i] = ob.elements[i];
   molecule = ob.molecule;
}

MolEnsemble & MolEnsemble::operator=(MolEnsemble & ob)
{
   if (this != &ob)
   {
      delete[]elements;
      totalnat = ob.totalnat;
      spectype = ob.spectype;
      elements = new AtomEnsemble[totalnat];
      for (int i = 0; i < totalnat; i++)
         elements[i] = ob.elements[i];
      molecule = ob.molecule;
   }
   return *this;
}

MolEnsemble::~MolEnsemble()
{
   delete[]elements;
}

void MolEnsemble::CalculateMasses()     //calculate masses of all molecules in the ensemble
{
   for (list < MolDistribution >::iterator i = molecule.begin();
        i != molecule.end(); i++)
   {
      for (unsigned int j = 0; j < i->atomlist.size(); j++)
      {
         i->atomlist[j]->CalculateMass(&elements[j].atomdef);
      }
      i->CalculateMass();
   }
}

ostream & operator<<(ostream & stream, MolEnsemble & ob)
{
   for (int i = 0; i < ob.totalnat; i++)
   {
      stream << ob.elements[i].atomdef;
   }
   for (list < MolDistribution >::iterator i = ob.molecule.begin();
        i != ob.molecule.end(); i++)
   {
      stream << *i;
   }
   return stream;
}

IsoCalc::~IsoCalc()
{
}

IsoCalc::IsoCalc():atomsloaded(false), molloaded(false), normalized(false), calculated(false), autocalc(false), thr(0.001), degen(0.1)
{
}

IsoCalc::IsoCalc(char *filename)        //constructor that reads in an atom table from a file
{
}

int IsoCalc::ReadAtomTable(void)        //read the atom table from a file, returns 0 on success
{

   string filename="calcisotope_table.txt";

   // check if the calcisotope_table.txt file exists
   FILE *fp;
   if ( (fp=fopen(filename.c_str(), "r")) == NULL)
      WriteTmpIsotopeTable();   // horrible hack to always write a tmp file 
   else
      fclose(fp);

   atomsloaded = false;         // reset this flag, in case the file open fails
   ifstream in(filename);
   if (!in)
      return 1;
   string buff;
   char sym[4];
   int _atno, _niso = 0;
   do
   {                            //do a first pass of the file to count how many isotopes for each element and
                                //create the necessary entries in the table in the same order that they are in the file
      in >> buff;
      if (isalpha(buff[0]))
      {
         if (_niso)
         {                      // need to check that we are not on the first entry in the file
            AtomIsoAbun temp(_atno, (_niso - 1) / 2);   //setup the new isotope abundance object and add it to the atomtable
            strcpy(temp.symbol, sym);
            if (find(atomtable.begin(), atomtable.end(), temp) !=
                atomtable.end())
               return 1;        //return an error if there are any duplicate definitions in the file
            atomtable.push_back(temp);
         }
         if (buff.size() < 4)
            strcpy(sym, buff.c_str());  //copy the symbol to another buffer
         else
            return 1;           // return an error if the symbol is too long (more than 3 characters)
         in >> _atno;
         if (in.rdstate() || _atno < 0)
            return 1;           //check for dodgy values
         _niso = 0;
      }
      _niso++;
   }
   while (!(in.rdstate() & ios::eofbit));
   AtomIsoAbun temp(_atno, (_niso - 1) / 2);    //deal with the case at the end of the file - where reads run into EOF instead of the next symbol
   strcpy(temp.symbol, sym);
   atomtable.push_back(temp);
   in.clear();
   in.seekg(0, ios_base::beg);  // do a second pass on the file to read in the details, return error 1 if anything goes wrong
   deque < AtomIsoAbun >::iterator j = atomtable.begin();
   do
   {
      in >> buff >> buff;       // 2 dummy reads to skip over the atom symbol and atomic number (already read these)
      if (in.rdstate())
         return 1;
      for (int i = 0; i < j->Getniso(); i++)
      {                         //read accurate mass and abundance values, then sort according to abundance
         in >> j->isotopes[i].mass;
         if (in.rdstate() || j->isotopes[i].mass < 0)
            return 1;           //dodgy value check for negative masses
         in >> j->isotopes[i].abundance;
         if (in.rdstate() || j->isotopes[i].abundance > 1
             || j->isotopes[i].abundance < 0)
            return 1;           //check for dodgy values of the isotope abundance
      }
      j->Sort_abundance();
      j++;
   }
   while (j != atomtable.end());
   atomsloaded = true;          //atom table loaded ok, so set the flag

   return 0;
}

void IsoCalc:: WriteTmpIsotopeTable(void)    // horrible hack to always write a tmp file 
{
   FILE *fp;

   if ( (fp = fopen("calcisotope_table.txt", "w")) == NULL)
   {
      printf(" Error cannot write calcisotope_table.txt file\n");
      exit(1);
   }

fprintf(fp, "\
H	1	1.007825032	0.999885\n\
		2.014101778	0.000115\n\
D	1	2.014101778	1\n\
T	1	3.016049268	1\n\
He	2	3.01602931	0.00000137\n\
		4.00260325	0.99999863\n\
Li	3	6.0151223	0.0759\n\
		7.016004	0.9241\n\
Be	4	9.0121821	1\n\
B	5	10.012937	0.199\n\
		11.0093055	0.801\n\
C	6	12	0.9893\n\
		13.00335484	0.0107\n\
C*	6	14.00324199	1\n\
N	7	14.00307401	0.99632\n\
		15.0001089	0.00368\n\
O	8	15.99491462	0.99757\n\
		16.9991315	0.00038\n\
		17.9991604	0.00205\n\
F	9	18.9984032	1\n\
Ne	10	19.99244018	0.9048\n\
		20.99384674	0.0027\n\
		21.99138551	0.0925\n\
Na	11	22.98976967	1\n\
Mg	12	23.9850419	0.7899\n\
		24.98583702	0.1\n\
		25.98259304	0.1101\n\
Al	13	26.98153844	1\n\
Si	14	27.97692653	0.922297\n\
		28.97649472	0.046832\n\
		29.97377022	0.030872\n\
P	15	30.97376151	1\n\
S	16	31.97207069	0.9493\n\
		32.9714585	0.0076\n\
		33.96786683	0.0429\n\
		35.96708088	0.0002\n\
Cl	17	34.96885271	0.7578\n\
		36.9659026	0.2422\n\
Ar	18	35.96754628	0.003365\n\
		37.9627322	0.000632\n\
		39.96238312	0.996003\n\
K	19	38.9637069	0.932581\n\
		39.96399867	0.000117\n\
		40.96182597	0.067302\n\
Ca	20	39.9625912	0.96941\n\
		41.9586183	0.00647\n\
		42.9587668	0.00135\n\
		43.9554811	0.02086\n\
		45.9536928	0.00004\n\
		47.952534	0.00187\n\
Sc	21	44.9559102	1\n\
Ti	22	45.9526295	0.0825\n\
		46.9517638	0.0744\n\
		47.9479471	0.7372\n\
		48.9478708	0.0541\n\
		49.9447921	0.0518\n\
V	23	49.9471628	0.0025\n\
		50.9439637	0.9975\n\
Cr	24	49.9460496	0.04345\n\
		51.9405119	0.83789\n\
		52.9406538	0.09501\n\
		53.9388849	0.02365\n\
Mn	25	54.9380496	1\n\
Fe	26	53.9396148	0.05845\n\
		55.9349421	0.91754\n\
		56.9353987	0.02119\n\
		57.9332805	0.00282\n\
Co	27	58.9332002	1\n\
Ni	28	57.9353479	0.680769\n\
		59.9307906	0.262231\n\
		60.9310604	0.011399\n\
		61.9283488	0.036345\n\
		63.9279696	0.009256\n\
Cu	29	62.9296011	0.6917\n\
		64.9277937	0.3083\n\
Zn	30	63.9291466	0.4863\n\
		65.9260368	0.279\n\
		66.9271309	0.041\n\
		67.9248476	0.1875\n\
		69.925325	0.0062\n\
Ga	31	68.925581	0.60108\n\
		70.924705	0.39892\n\
Ge	32	69.9242504	0.2084\n\
		71.9220762	0.2754\n\
		72.9234594	0.0773\n\
		73.9211782	0.3628\n\
		75.9214027	0.0761\n\
As	33	74.9215964	1\n\
Se	34	73.9224766	0.0089\n\
		75.9192141	0.0937\n\
		76.9199146	0.0763\n\
		77.9173095	0.2377\n\
		79.9165218	0.4961\n\
		81.9167	0.0873\n\
Br	35	78.9183376	0.5069\n\
		80.916291	0.4931\n\
Kr	36	77.920386	0.0035\n\
		79.916378	0.0228\n\
		81.9134846	0.1158\n\
		82.914136	0.1149\n\
		83.911507	0.57\n\
		85.9106103	0.173\n\
Rb	37	84.9117893	0.7217\n\
		86.9091835	0.2783\n\
Sr	38	83.913425	0.0056\n\
		85.9092624	0.0986\n\
		86.9088793	0.07\n\
		87.9056143	0.8258\n\
Y	39	88.9058479	1\n\
Zr	40	89.9047037	0.5145\n\
		90.905645	0.1122\n\
		91.9050401	0.1715\n\
		93.9063158	0.1738\n\
		95.908276	0.028\n\
Nb	41	92.9063775	1\n\
Mo	42	91.90681	0.1484\n\
		93.9050876	0.0925\n\
		94.9058415	0.1592\n\
		95.9046789	0.1668\n\
		96.906021	0.0955\n\
		97.9054078	0.2413\n\
		99.907477	0.0963\n\
Tc	43	96.906365	0\n\
		97.907216	1\n\
		98.9062546	0\n\
Ru	44	95.907598	0.0554\n\
		97.905287	0.0187\n\
		98.9059393	0.1276\n\
		99.9042197	0.126\n\
		100.9055822	0.1706\n\
		101.9043495	0.3155\n\
		103.90543	0.1862\n\
Rh	45	102.905504	1\n\
Pd	46	101.905608	0.0102\n\
		103.904035	0.1114\n\
		104.905084	0.2233\n\
		105.903483	0.2733\n\
		107.903894	0.2646\n\
		109.905152	0.1172\n\
Ag	47	106.905093	0.51839\n\
		108.904756	0.48161\n\
Cd	48	105.906458	0.0125\n\
		107.904183	0.0089\n\
		109.903006	0.1249\n\
		110.904182	0.128\n\
		111.9027572	0.2413\n\
		112.9044009	0.1222\n\
		113.9033581	0.2873\n\
		115.904755	0.0749\n\
In	49	112.904061	0.0429\n\
		114.903878	0.9571\n\
Sn	50	111.904821	0.0097\n\
		113.902782	0.0066\n\
		114.903346	0.0034\n\
		115.901744	0.1454\n\
		116.902954	0.0768\n\
		117.901606	0.2422\n\
		118.903309	0.0859\n\
		119.9021966	0.3258\n\
		121.9034401	0.0463\n\
		123.9052746	0.0579\n\
Sb	51	120.903818	0.5721\n\
		122.9042157	0.4279\n\
Te	52	119.90402	0.0009\n\
		121.9030471	0.0255\n\
		122.904273	0.0089\n\
		123.9028195	0.0474\n\
		124.9044247	0.0707\n\
		125.9033055	0.1884\n\
		127.9044614	0.3174\n\
		129.9062228	0.3408\n\
I	53	126.904468	1\n\
Xe	54	123.9058958	0.0009\n\
		125.904269	0.0009\n\
		127.9035304	0.0192\n\
		128.9047795	0.2644\n\
		129.9035079	0.0408\n\
		130.9050819	0.2118\n\
		131.9041545	0.2689\n\
		133.9053945	0.1044\n\
		135.90722	0.0887\n\
Cs	55	132.905447	1\n\
Ba	56	129.90631	0.00106\n\
		131.905056	0.00101\n\
		133.904503	0.02417\n\
		134.905683	0.06592\n\
		135.90457	0.07854\n\
		136.905821	0.11232\n\
		137.905241	0.71698\n\
La	57	137.907107	0.0009\n\
		138.906348	0.9991\n\
Ce	58	135.90714	0.00185\n\
		137.905986	0.00251\n\
		139.905434	0.8845\n\
		141.90924	0.11114\n\
Pr	59	140.907648	1\n\
Nd	60	141.907719	0.272\n\
		142.90981	0.122\n\
		143.910083	0.238\n\
		144.912569	0.083\n\
		145.913112	0.172\n\
		147.916889	0.057\n\
		149.920887	0.056\n\
Pm	61	144.912744	0\n\
		146.915134	0\n\
Sm	62	143.911995	0.0307\n\
		146.914893	0.1499\n\
		147.914818	0.1124\n\
		148.91718	0.1382\n\
		149.917271	0.0738\n\
		151.919728	0.2675\n\
		153.922205	0.2275\n\
Eu	63	150.919846	0.4781\n\
		152.921226	0.5219\n\
Gd	64	151.919788	0.002\n\
		153.920862	0.0218\n\
		154.922619	0.148\n\
		155.92212	0.2047\n\
		156.923957	0.1565\n\
		157.924101	0.2484\n\
		159.927051	0.2186\n\
Tb	65	158.925343	1\n\
Dy	66	155.924278	0.0006\n\
		157.924405	0.001\n\
		159.925194	0.0234\n\
		160.92693	0.1891\n\
		161.926795	0.2551\n\
		162.928728	0.249\n\
		163.929171	0.2818\n\
Ho	67	164.930319	1\n\
Er	68	161.928775	0.0014\n\
		163.929197	0.0161\n\
		165.93029	0.3361\n\
		166.932045	0.2293\n\
		167.932368	0.2678\n\
		169.93546	0.1493\n\
Tm	69	168.934211	1\n\
Yb	70	167.933894	0.0013\n\
		169.934759	0.0304\n\
		170.936322	0.1428\n\
		171.9363777	0.2183\n\
		172.9382068	0.1613\n\
		173.9388581	0.3183\n\
		175.942568	0.1276\n\
Lu	71	174.9407679	0.9741\n\
		175.9426824	0.0259\n\
Hf	72	173.94004	0.0016\n\
		175.9414018	0.0526\n\
		176.94322	0.186\n\
		177.9436977	0.2728\n\
		178.9458151	0.1362\n\
		179.9465488	0.3508\n\
Ta	73	179.947466	0.00012\n\
		180.947996	0.99988\n\
W	74	179.946706	0.0012\n\
		181.948206	0.265\n\
		182.9502245	0.1431\n\
		183.9509326	0.3064\n\
		185.954362	0.2843\n\
Re	75	184.9529557	0.374\n\
		186.9557508	0.626\n\
Os	76	183.952491	0.0002\n\
		185.953838	0.0159\n\
		186.9557479	0.0196\n\
		187.955836	0.1324\n\
		188.9581449	0.1615\n\
		189.958445	0.2626\n\
		191.961479	0.4078\n\
Ir	77	190.960591	0.373\n\
		192.962924	0.627\n\
Pt	78	189.95993	0.00014\n\
		191.961035	0.00782\n\
		193.962664	0.32967\n\
		194.964774	0.33832\n\
		195.964935	0.25242\n\
		197.967876	0.07163\n\
Au	79	196.966552	1\n\
Hg	80	195.965815	0.0015\n\
		197.966752	0.0997\n\
		198.968262	0.1687\n\
		199.968309	0.231\n\
		200.970285	0.1318\n\
		201.970626	0.2986\n\
		203.973476	0.0687\n\
Tl	81	202.972329	0.29524\n\
		204.974412	0.70476\n\
Pb	82	203.973029	0.014\n\
		205.974449	0.241\n\
		206.975881	0.221\n\
		207.976636	0.524\n\
Bi	83	208.980383	1\n\
Po	84	208.982416	1\n\
		209.982857	0\n\
At	85	209.987131	1\n\
		210.987481	0\n\
Rn	86	210.990585	0\n\
		220.0113841	0\n\
		222.0175705	1\n\
Fr	87	223.0197307	1\n\
Ra	88	223.018497	0\n\
		224.020202	0\n\
		226.0254026	1\n\
		228.0310641	0\n\
Ac	89	227.027747	1\n\
Th	90	230.0331266	0\n\
		232.0380504	1\n\
Pa	91	231.0358789	1\n\
U	92	233.039628	0\n\
		234.0409456	0.000055\n\
		235.0439231	0.0072\n\
		236.0455619	0\n\
		238.0507826	0.992745\n\
Np	93	237.0481673	1\n\
		239.0529314	0\n\
Pu	94	238.0495534	0\n\
		239.0521565	0\n\
		240.0538075	0\n\
		241.0568453	0\n\
		242.0587368	0\n\
		244.064198	1\n\
Am	95	241.0568229	0\n\
		243.0613727	1\n\
Cm	96	243.0613822	0\n\
		244.0627463	0\n\
		245.0654856	0\n\
		246.0672176	0\n\
		247.070347	1\n\
		248.072342	0\n\
Bk	97	247.070299	1\n\
		249.07498	0\n\
Cf	98	249.074847	0\n\
		250.0764	0\n\
		251.07958	1\n\
		252.08162	0\n\
Es	99	252.08297	1\n\
Fm	100	257.095099	1\n\
Md	101	256.09405	0\n\
		258.098425	1\n\
No	102	259.10102	1\n\
Lr	103	262.10969	1\n\
Rf	104	261.10875	1\n\
Db	105	262.11415	1\n\
Sg	106	266.12193	1\n\
Bh	107	264.12473	1\n\
Hs	108	277	1\n\
Mt	109	268.13882	1\n");

   fclose(fp);
}

int IsoCalc::SetComposition(char *formula)      //convert a string containing a molecular formula to a MolComposition, returns 0 on success
{
   if (!atomsloaded)
      return 3;
   molloaded = false;           //reset in case composition assignment fails
   calculated = false;
   normalized = false;
   if (!strlen(formula))
      return 2;
   string trimformula;  //create a new string containing the formula with white space stripped out
   char  *ptr = formula;
   while (*ptr != '\0')
   {
      if (!isspace(*ptr))
         trimformula += *ptr;
      ptr++;
   }
   set < string > atomtypes;    // a set containing the different atom types - do it this way to cope with formulas like C6H5OH where atoms are specified multiple times
   string currelement;
   size_t loc = 0;
   while (loc != trimformula.size())
   {
      if (isupper(trimformula[loc]))
      {                         // if current character is a capital letter - then it is the start of a new element
         if (currelement.size())
         {
            atomtypes.insert(currelement);      //add the previous element (if there was one) to the set
         }
         currelement = trimformula[loc];        //add the new character to the current element
      }
      else if (islower(trimformula[loc]) || trimformula[loc] == '*')
      {                         //append any lower case characters or the * character to indicate a label
         currelement += trimformula[loc];
      }
      loc++;
   }
   atomtypes.insert(currelement);       //add the final element
   molecule.Settotalnat(atomtypes.size());      //set the number of atomtypes
   set < string >::iterator i = atomtypes.begin();
   int c = 0;
   while (i != atomtypes.end())
   {                            //look up up atomic isotope distributions for each element
      deque < AtomIsoAbun >::iterator j = atomtable.begin();
      while (j != atomtable.end() && strcmp(j->symbol, i->c_str()))
      {
         j++;
      }
      if (j != atomtable.end())
      {
         molecule.nat[c] = 0;
         molecule.atom[c] = *j;
      }
      else
         return 2;
      c++;
      i++;
   }
   currelement = "";
   string number;       // the number of atoms
   loc = 0;
   while (loc != trimformula.size())
   {                            //do a second parse on the formula to tally up the number of each type of atom
      if (isupper(trimformula[loc]))
      {                         // if current character is a capital letter - then it is the start of a new element
         if (currelement.size())
         {                      //if it is not the very first character, add on the appropriate number of atoms
            c = 0;
            while (strcmp(molecule.atom[c].symbol, currelement.c_str()))
            {
               c++;
            }                   //look up where to add to
            molecule.nat[c] += number.size()? atoi(number.c_str()) : 1; // because CH4 means C1H4
         }
         currelement = trimformula[loc];        //add the new character to the current element
         number = "";           //zero the number
      }
      else if (islower(trimformula[loc]) || trimformula[loc] == '*')
      {                         //append any lower case characters or the * character to indicate a label
         currelement += trimformula[loc];
      }
      else if (isdigit(trimformula[loc]))
      {
         number += trimformula[loc];
      }
      else
      {
         return 2;
      }
      loc++;
   }
   c = 0;
   while (strcmp(molecule.atom[c].symbol, currelement.c_str()))
   {
      c++;
   }                            // deal with the final element - look up where to add to
   molecule.nat[c] += number.size()? atoi(number.c_str()) : 1;  // because CH4 means C1H4
   molloaded = true;
   if (autocalc)
      return Calculate();       //if autocalculate, then need to recalculate the distribution now
   return 0;
}

int IsoCalc::Calculate()        //calculate the isotope distribution AND spectrum, return 0 on success
{
   int errnr;
   if (!atomsloaded)
      return 3;                 //abort if there is no data or molecule
   if (!molloaded)
      return 4;
   calculated = false;          //in case anything goes wrong
   normalized = false;
   errnr = ensemble.GenerateEnsemble(molecule, thr);
   if (errnr)
      return errnr;
   ensemble.MakeSpectrum(&masspeaks, degen);
   calculated = true;
   return 0;
}

int IsoCalc::GetNPeaks(int &_npeaks)    //return the number of peaks in the peaklist
{
   if (!calculated)
   {
      _npeaks = 0;
   }                            // could return an error 5 here, but simply give 0 as the number of peaks
   _npeaks = masspeaks.size();
   return 0;
}

int IsoCalc::Mass(int n, double &m)     //mass (m) of peak no. n (zero based indexing), return 0 on success
{
   if (!calculated)
   {
      m = 0;
      return 5;
   }
   if (n < 0 || n >= masspeaks.size())
   {
      m = 0;
      return 6;
   }
   m = masspeaks[n].mass;
   return 0;
}

int IsoCalc::Abundance(int n, double &abun)     //abundance of peak n (zero based indexing, return 0 on success
{
   if (!calculated)
   {
      abun = 0;
      return 5;
   }
   if (n < 0 || n >= masspeaks.size())
   {
      abun = 0;
      return 6;
   }
   abun = masspeaks[n].abundance;
   return 0;
}

int IsoCalc::Peak(int n, double &mass, double &abun)    //mass and abundance of peak number n (zero based indexing)
{                               //return 0 on success
   if (!calculated)
   {
      mass = 0;
      abun = 0;
      return 5;
   }                            //give both mass and abundance as zero, and return an error
   if (n < 0 || n >= masspeaks.size())
   {
      mass = 0;
      abun = 0;
      return 6;
   }
   mass = masspeaks[n].mass;
   abun = masspeaks[n].abundance;
   return 0;
}

int IsoCalc::Normalize()        // normalize the intensity so that most intense peak = 100%
{
   if (!calculated)
      return 5;
   double max = 0;
   for (int i = 0; i < masspeaks.size(); i++)
   {
      if (masspeaks[i].abundance > max)
         max = masspeaks[i].abundance;
   }
   for (int i = 0; i < masspeaks.size(); i++)
   {
      masspeaks[i].abundance = 100 * masspeaks[i].abundance / max;
   }
   normalized = true;
   return 0;
}

int IsoCalc::SetFwhm(const double _fwhm)        //setter for the peak full width at half max, return 0 on success
{
   if (_fwhm <= 0)
      return 7;
   masspeaks.fwhm = _fwhm;
   return 0;
}

int IsoCalc::GetFwhm(double &_fwhm)     //getter for the peak full width at half max, return 0 on success
{
   _fwhm = masspeaks.fwhm;
   return 0;
}

int IsoCalc::SetThr(const double _thr)  // setter for isotope calculation threshold
{
   if (_thr < 0 || thr >= 1)
      return 7;
   thr = _thr;
   if (autocalc)
      Calculate();              //if autocalculate flag is set then will need to redo the isotope calculate
   return 0;
}

int IsoCalc::GetThr(double &_thr)       //getter for isotope calculation threshold
{
   _thr = thr;
   return 0;
}

int IsoCalc::Intensity(double m, double &i)     //supplies intensity i at mass m calculated using gaussians
{
   if (!calculated)
   {
      i = 0;
      return 5;
   }                            // give zero intensity, and return an error
   i = masspeaks.intensity(m);
   return 0;
}

int IsoCalc::SetAutoCalc(const bool _autocalc)  //setter for the autocalc flag
{
   autocalc = !autocalc;
   if (autocalc)
   {                            // update the calculation if required
      if (!calculated)
         Calculate();
   }
   return 0;
}

int IsoCalc::GetAutoCalc(bool & _autocalc)
{
   _autocalc = autocalc;
   return 0;
}

int IsoCalc::SetCharge(const int _z)    //update charge
{
   molecule.z = _z;
   if (autocalc)
      Calculate();
   return 0;
}

int IsoCalc::GetCharge(int &_z) //return charge
{
   _z = molecule.z;
   return 0;
}

int IsoCalc::SetDegen(const double _degen)
{
   if (degen <= 0)
      return 7;
   degen = _degen;
   if (autocalc)
      Calculate();              //if autocalculate flag is set then will need to redo the isotope calculate
   return 0;
}

int IsoCalc::GetDegen(double &_degen)
{
   _degen = degen;
   return 0;
}

int IsoCalc::SetMassToCharge(const bool _mtoz)  //sets the spectrum type
{
   if (_mtoz)
      ensemble.spectype = MolEnsemble::masstocharge;
   else
      ensemble.spectype = MolEnsemble::mass;
   if (autocalc)
   {                            // update the calculation if required
      if (!calculated)
         Calculate();
   }
   return 0;
}

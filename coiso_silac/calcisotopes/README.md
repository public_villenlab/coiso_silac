Isotope Distribution Calculator

Copyright 2019 James E. Redman

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1.	Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
2.	Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
3.	Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.

IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The 64 bit version requires a compatible processor and a 64 bit version of Windows.

This is a Windows console application for calculating isotope distributions for molecules. It is designed to work with both small molecules and proteins. The element definitions are provided in a separate file, which can easily be edited to allow for isotopically labelled compounds. The output is to the console by default, but can be redirected to a file, and consists of pairs of mass and abundance values.

To install the software, simply extract the files from the zip archive (right click the icon, then select **Extract All...** from the menu). To uninstall, just delete the files.

To run, start up the windows command prompt (under accessories on the Start Menu on XP, or type cmd in the search box on later versions), then cd to the directory where you unpacked the files. Then type:

```
isotopes formula
```

where formula is the molecular formula of interest. It currently understands "standard" elements, but not any other chemical abbreviations. Valid formulae could include C254H377N65O75S6 for insulin and C2H6O for ethanol. CH3CH2OH will work too. Parentheses are not yet supported.

There are various other options:
```
isotopes formula filename.txt
```

The optional filename species that the program should use a different atom definition file. The file format is straightforward, and contains the element symbol, followed by atomic number and then pairs of mass and abundance values (tab separated) for each isotope.


```
-n
Normalizes the most intense peak to 100%, as is conventional for mass spectra
-f filt		Normalizes and discards peaks of intensity < filt
-t thr		Specifies a probability threshold thr, defaults to 0.001. Lower thresholds will find more peaks (and take longer to calculate)
-p prec		Specifies the output precision prec, default is 8
-c		Outputs a comma separated table. The default is tab
-r res		Peaks separated by <res will be merged. Defaults to 0.1
-z charge		Specifies the charge, defaults to 0
-mtz charge		Outputs mass-to-charge ratio and sets charge. By default outputs mass
```


Any unrecognised options will simply be ignored. In the event that the program cannot calculate the distribution an error message will result.

Examples:

```isotopes C254H377N65O75S6``` 

Calculates isotope distribution for insulin (bovine) using default settings.


```isotopes C254H377N65O75S6 -f 1```  

Calculates isotope distribution for insulin, normalizes and outputs peaks of intensity >=1%.

```isotopes C254H378N65O75S6 -f 1 –mtz 1```  

Calculates mass-to-charge ratio for protonated singly charged insulin, normalizes and outputs peaks of intensity >=1%.


```isotopes C254H377N65O75S6 -n -p 5 -c > insulin.csv``` 

This calculates the insulin isotope distribution, outputting 5 figures of precision, normalizing the most intense peak to 100%, and sends the results to a comma separated file called insulin.csv (which will open in MS Excel if this is installed).


```isotopes C254H377N65O75S6 -t 0.00001```  
Calculates the distribution, but will consider less likely isotopomers than with the default thr setting of 0.001


The algorithm used is based on a published procedure (J. A. Yergey, Int. J. Mass Spectrom. Ion Phys. 1983, 52, 337) but is implemented rather differently from the way described in the paper, and will not give numerically identical results.

If specifying a charged molecule with the -z or -mtz options this adds or subtracts electron(s) only. If you are interested in calculating electrospray or MALDI mass spectra you will need to adjust the number of hydrogens too if the charge on the ion of interest is provided by protonation. The -z and –mtz options will give the same result when the charge is +1.

The values for masses and abundances in the supplied file isotopestable.txt are those given by NIST.

Coursey, J.S., Schwab, D.J., Tsai, J.J., and Dragoset, R.A. (2015), Atomic Weights and Isotopic Compositions (version 4.1). [Online] Available: http://physics.nist.gov/Comp [2019, 08, 08]. National Institute of Standards and Technology, Gaithersburg, MD.
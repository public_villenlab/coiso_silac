//
// USAGE:  ./calcisotopes C12H24NOS
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>

#include "isotopes.h"

int main(int argc, char **argv)
{
   if (argc<2)
   {
      printf("\n");
      printf(" USAGE:  %s <composition>\n", argv[0]);
      printf("         %s C12N24HOS\n", argv[0]);
      printf("\n");
      printf(" Isotope distribution calculation code Copyright 2013 James E. Redmon\n");
      printf(" Based on J.A.Yergey, Int. J. Mass Spectrom. Ion Phys. 1983, 52, 337-349\n");
      printf("\n");
      exit(1);
   }

   char szComp[128];
   char szTmp[128];
   int z = 1;
   int prec = 2;
   bool masstocharge = true;
   double thr = 0.0001, res = 0.01, filter = 0;        //do not output any peaks of intensity < filter

   strcpy(szComp, argv[1]);

   if (szComp[0] == '\'')
   {
      strcpy(szTmp, szComp+1);
      strcpy(szComp, szTmp);
   }
   if (szComp[strlen(szComp)-1] == '\'')
   {
      szComp[strlen(szComp)-1] = '\0';
   }


   IsoCalc pTmp;
   pTmp.WriteTmpIsotopeTable();

   IsoCalc mycalc;
   int errnr = mycalc.ReadAtomTable();
   if (errnr)
   {
      cout << "Error: cannot read isotope table.\n";
      exit(1);
   }

   errnr = mycalc.SetComposition(szComp);
   if (errnr)
   {
      cout << "Error: cannot parse formula\n";
      exit(1);
   }

   mycalc.SetCharge(z);
   mycalc.SetThr(thr);
   mycalc.SetMassToCharge(masstocharge);
   mycalc.SetDegen(res);
   cout.precision(prec);     // set the precision
   errnr = mycalc.Calculate();
   if (errnr)
   {
      cout << "Error: cannot calculate distributions\n";
      exit(1);
   }                         // this should never happen - but just in case
   
   mycalc.Normalize();       // normalize max intensity to 100
   
   int npeaks=0;
   double mass, abun;
   mycalc.GetNPeaks(npeaks);
   int iMaxIsotope = 0;
   double dMaxAbun = 0;
   double dAbunMono = 1;
   
   if (npeaks > 5)
      npeaks = 5;

   printf("[");
   // write isotope distribution
   for (int i = 0; i < npeaks; i++)
   {
      mycalc.Peak(i, mass, abun);
   
      if (i==0)
         dAbunMono = abun;
   
      if (i > 0)
         printf(",");

      if (npeaks < i)
         printf("0.0");
      else
         printf("%0.2f", 100.0 * abun / dAbunMono);
   }
   if (npeaks < 5)
   {
      for (int i = npeaks ; i < 5 ; i++)
         printf(",0.0");
   }

   printf("]\n");
   fflush(stdout);

   exit(0);
}

# Coiso SILAC pipeline

## Introduction

The **coiso_silac** package provides users the proteomics software suite an all-in-one Coiso SILAC analysis, which includes the following:

1.	Filter peptide-spectral matches (PSM) at 1% FDR using mokapot (Fondrie & Noble J. Proteome Research 2021)
2.	Annotate PSM precursor and peptide fragment ions from .mzML spectra (MS/MS for K0K8; MS1 and MS/MS for K6K8)
3.	Provide MS/MS-based Coiso SILAC quantification for K0K8 and K6K8 samples (K0K8 paired y-ion; K6K8 deconvoluted paired y-ion)
4.	K6K8 MS/MS-based quantification via paired y-ion deconvolution and MS1 peptide pair deconvolution using the mathematical model from Chavez et al. (Analytical Chem. 2020)
5.	K0K8 MS1 feature mapping from standard Dinosaur (Teleman et al. J. Proteome Research 2016) output .features.tsv to PSM identifications


## Getting Started

### Install from GitLab

If you just want to use the coiso_silac package, and don't want to contribute, you can get the most up to date version straight from this repository with pip.

```
pip install git+https://gitlab.com/public_villenlab/coiso_silac.git
```


### Usage

The coiso_silac package can be used straight from the command line. 
A full list of parameters is available by running with the `-h` flag.

coiso_silac has the following inputs:

1.	QUANT_TYPE : either "K0K8" or "K6K8"
2.	MZML_FILE : spectral data from msConvert ending in .mzML
3.	PIN_FILE : peptide identification file from Comet (.pin file). To obtain from Comet make sure the parameter is "output_percolatorfile = 1"
4.	DINOSAUR_FILE : only for K0K8 quantification (ignore -d command for K0K8). This file extension is a .features.tsv and is the standard format when .mzML file is run through Dinosaur (Teleman et al. J. Proteome Research 2016)
5.	OUTPUT_NAME : user defined name of output qunatification file which summarizes the Coiso SILAC analysis as .csv file

Of note, K0K8 requires dinosaur_file for feature mapping, while K6K8 MS1 quantification is derived using the coiso_silac package (not requiring that "-d" input).

Current implementation for K0K8 is amenable to different modifications, however certain N-terminal modifications might result in errors.

Current implementation is only enabled for K0K8 and K6K8 with the following modifications and LysC digestion:

K0K8:
1.	Static modification Carbidomethylation of Cysteines [+57.021464]
2.	Variable N-terminal Peptide Acetylation [+42.01056]
3.	Variable Oxidation of Methionine [+15.9949]
4.	Variable Heavy Lysine [+8.014199]
5.	Amenable to other delta mass modifications (however, not on lysines or N-terminus)



K6K8:
1.	Static Carbidomethylation of Cysteines [+57.021464]
2.	Static Lys6 Light Lysine [+6.020129]
3.	Variable N-terminal Peptide Acetylation [+42.01056]
4.	Variable Oxidation of Methionine [+15.9949]
5.	Variable Lys8 Heavy Lysine[+1.99407]
6.	NOT amenable to other delta mass modifications (however, working on enabling this feature for future versions)


```
$ coiso_silac -h

usage: coiso_silac [-h] -q QUANT_TYPE -m MZML_FILE -p PIN_FILE [-d DINOSAUR_FILE] -o OUTPUT_NAME

Coiso SILAC Quantification Tool

optional arguments:
  -h, --help            show this help message and exit
  -q QUANT_TYPE, --quant_type QUANT_TYPE
                        Designate K0K8 or K6K8 SILAC labels used (default: None)
  -m MZML_FILE, --mzML_file MZML_FILE
                        .mzML file for spectral processing (default: None)
  -p PIN_FILE, --pin_file PIN_FILE
                        Coiso Comet .pin filename (default: None)
  -d DINOSAUR_FILE, --dinosaur_file DINOSAUR_FILE
                        Dinosaur .feature.tsv filename - K0K8 only (default: None)
  -o OUTPUT_NAME, --output_name OUTPUT_NAME
                        Quant output filename : name as .csv (default: None)
```

Example K0K8 run:

```
$ coiso_silac -q K0K8 -m filename.mzML -p filename.pin -d filename.features.tsv -o userOutputName.csv
```


Example K6K8 run:

```
$ coiso_silac -q K6K8 -m filename.mzML -p filename.pin -o userOutputName.csv
```


References:

Chavez, J.D., Keller, A., Mohr, J.P., Bruce, J.E., 2020. Isobaric Quantitative Protein Interaction Reporter Technology for Comparative Interactome Studies. Anal. Chem. 92, 14094–14102. https://doi.org/10.1021/acs.analchem.0c03128

Fondrie, W.E., Noble, W.S., 2021. mokapot: Fast and Flexible Semisupervised Learning for Peptide Detection. J. Proteome Res. 20, 1966–1971. https://doi.org/10.1021/acs.jproteome.0c01010

Teleman, J., Chawade, A., Sandin, M., Levander, F., Malmström, J., 2016. Dinosaur: A Refined Open-Source Peptide MS Feature Detector. J. Proteome Res. 15, 2143–2151. https://doi.org/10.1021/acs.jproteome.6b00016

Isotope distribution calculation code Copyright 2013 James E. Redmon (README.md for license in calcisotopes directory)

Coursey, J.S., Schwab, D.J., Tsai, J.J., and Dragoset, R.A. (2015), Atomic Weights and Isotopic Compositions (version 4.1). [Online] Available: http://physics.nist.gov/Comp [2019, 08, 08]. National Institute of Standards and Technology, Gaithersburg, MD.

Based on Yergey, James., Heller, David., Hansen, Gordon., Cotter, R.J., Fenselau, Catherine., 1983. Isotopic distributions in mass spectra of large molecules. Anal. Chem. 55, 353–356. https://doi.org/10.1021/ac00253a037

